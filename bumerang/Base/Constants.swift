
import Foundation
import Firebase

struct Constants {
    
    static let SAVE_ROOT_PATH = "rms"
    
    struct refs {
        static let databaseRoot = Database.database().reference()

    }
    
    static let userType = ["standard", "business"]
    static let userTypeAPI = ["Standard", "Business"]
    static let mailType = ["Email", "Facebook", "Google"]
    
    static let categoryName = [
        "Flat, Aparment", "Rental Car", "Caravan", "Sea Vehicle", "Dress",
        "Bike", "Camera", "Spore", "Kamp", "Nusic", "Other"
    ]
    
    static let cateImage = [
        "ic_flat_2",
        "ic_car_2",
        "ic_caravan_2",
        "ic_seavehicle_2",
        "ic_dress_2",
        "ic_bike_2",
        "ic_camera_2",
        "ic_spore_2",
        "ic_camp_2",
        "ic_music_2",
        "ic_others_2"
    ]
    
    static let cataImg = [
        "ic_house1",    "ic_car",
        "ic_caravan",   "ic_sea_vehicle",
        "ic_dress_red", "ic_bike",
        "ic_camera",    "ic_spore",
        "ic_kamp",      "ic_music",
        "ic_other"
    ]
    
    static let cataColor = [
        0xA686FD, 0x99A9B3,
        0xB9D8CD, 0x68A4CC,
        0xE4B1B2, 0xE5F66F,
        0xC089E1, 0x9BB9F0,
        0x67D031, 0xB02774,
        0x885353
    ]
    
    static let currencyUnit = "$"
    static let requestStateText = [
        "New Request",
        "Accepted Request",
        "Under Rent",
        "Cancelled Request",
        "Finished Request"
    ]
    
    static let arrPlaceHolerImage = [
        "default_house_img",
        "default_car_img",
        "default_car_img",
        "default_house_img",
        "default_dress_img",
        "default_bike_img",
        "default_camera_img",
        "default_house_img",
        "default_house_img",
        "default_house_img",
        "default_house_img"
    ]
    
}

