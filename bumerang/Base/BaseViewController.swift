
import UIKit
import SwiftyUserDefaults
import Toast_Swift
import NotificationCenter

class BaseViewController: UIViewController {

    var darkView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    func gotoMainBeforeVC () {
        
        self.storyboard?.instantiateInitialViewController()?.dismiss(animated: true, completion: nil)
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "MainbeforeLoginVC")
        
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!, animated: false, completion: nil)
    }
    
    
    func gotoMainAfterVC () {
         self.storyboard?.instantiateInitialViewController()?.dismiss(animated: true, completion: nil)
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "MainpageAfterNav")
        
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!, animated: false, completion: nil)
    }
    
    // dissmis current VC and Goto one VC
    
    func dissmisAndGotoVC (_ nameVC: String) {
        
        self.dismiss()
        self.GotoNewVC(nameVC)
    }
    
    //Goto new VC
    func GotoNewVC (_ nameVC: String) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: nameVC)
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: false, completion: nil)
    }
    //dissmis current VC
    func dismiss() {
        self.storyboard?.instantiateInitialViewController()?.dismiss(animated: true, completion: nil)
    }
    
    // load one vc on current NavigationScreen
    func gotoNavigationScreen(_ nameVC: String, direction : CATransitionSubtype) {
        
        setTransitionType(direction)
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: nameVC)
        
        toVC!.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(toVC!, animated: true)
    }
    
    //set dispaly effect
    func setTransitionType(_ direction : CATransitionSubtype) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.moveIn
        transition.subtype = direction
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        
        view.window!.layer.add(transition, forKey: kCATransition)
    }
    
    func setTextFieldOfAlertdialog(textField : UITextField, placeHolder :String){
        
        textField.placeholder = placeHolder
        textField.keyboardType = .numberPad
        textField.isSecureTextEntry = true
        
    }
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //preferredStyle: .actionSheet
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        DispatchQueue.main.async(execute:  {
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    func showError(_ message: String!) {

        showAlertDialog(title: R_EN.string.ERROR, message: message, positive: R_EN.string.OK, negative: nil)
    }

    func showAlert(_ title: String!) {
        showAlertDialog(title: title, message: "", positive: R_EN.string.OK, negative: nil)
    }
    
    func showHUD() {
        
        showHUDWithTitle(title: "")
    }
    
    func showHUDWithTitle(title: String) {
        
        if title == "" {
            ProgressHUD.show()
        } else {
            ProgressHUD.showWithStatus(string: title)
        }
    }
    
    func showSuccess() {
        
        DispatchQueue.main.async(execute:  {
            ProgressHUD.showSuccessWithStatus(string: R_EN.string.SUCCESS)
        })
    }
    
    // hide loading view
    func hideHUD() {
        
        ProgressHUD.dismiss()
    }
    
    func showToast(_ message : String) {
        self.view.makeToast(message)
    }
    
    func showToast(_ message : String, duration: TimeInterval = ToastManager.shared.duration, position: ToastPosition = ToastManager.shared.position) {
    
        self.view.makeToast(message, duration: duration, position: position)
    }
    
    var blackView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints =  false
        view.backgroundColor = UIColor.init(white: 0.70, alpha: 0.5)
        view.backgroundColor = UIColor.black
        
        return view
    }()
    
    func showToastProgress(){
        
        //self.navigationController?.isNavigationBarHidden = true
        
        darkView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        self.view.addSubview(darkView)
        self.view.makeToastActivity(ToastPosition.center)
    }
    
    func hideToastProgress(){
        //self.navigationController?.isNavigationBarHidden = false
        darkView.removeFromSuperview()
        self.view.hideToastActivity()
    }
}

extension DefaultsKeys {

    static let userId = DefaultsKey<Int>("userId", defaultValue: 0)
    static let email = DefaultsKey<String?>("email", defaultValue: "")
    static let pwd = DefaultsKey<String?>("pwd")
    static let firstname = DefaultsKey<String?>("firstname")
    static let lastname = DefaultsKey<String?>("lastname")
    static let userType = DefaultsKey<String?>("userType")
    static let registerState = DefaultsKey<Bool>("registerState", defaultValue: false)
    static let rememberState = DefaultsKey<Bool?>("rememberState", defaultValue: false)
 
}

