//
//  AppDelegate.swift
//  bumerang
//
//  Created by RMS on 2019/9/3.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Photos

import Firebase
import FBSDKCoreKit
import GoogleSignIn

let FB_SCHEME = "fb516615525799024"
let GOOGLE_SCHEME  = "com.googleusercontent.apps.480946499113-6deojl08i8420vph3crmcsba8t2rv7rc"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    

    
    
    

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
     
        // for google signin
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
        //GIDSignIn.sharedInstance()?.delegate = self
        
        // get permission of camera to take pictures
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied: break
        case .authorized: break
        case .restricted: break
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                if granted {
                    print("Granted access to \(cameraMediaType)")
                } else {
                    print("Denied access to \(cameraMediaType)")
                }
            }
        default:
            break
        }
        
        // permission for photo library
        let photoLibraryStatus = PHPhotoLibrary.authorizationStatus()
        
        switch photoLibraryStatus {
            case .authorized:
                break
            //handle authorized status
            case .denied, .restricted :
                break
            //handle denied status
            case .notDetermined:
                // ask for permissions
                PHPhotoLibrary.requestAuthorization() { status in
                    switch status {
                    case .authorized:
                        break
                    // as above
                    case .denied, .restricted:
                        break
                    // as above
                    case .notDetermined:
                        break
                        // won't happen but still
                    default: break
                    }
                }
            default: break
        }
        

        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if url.scheme == FB_SCHEME {
            return ApplicationDelegate.shared.application(
                application,
                open: url,
                sourceApplication: sourceApplication,
                annotation: annotation
            )
        } else {
            
            return (GIDSignIn.sharedInstance()?.handle(url))!
        }
        
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        
        if url.scheme == FB_SCHEME {
            return ApplicationDelegate.shared.application(application, open: url, options: options)
        } else {
            
            return (GIDSignIn.sharedInstance()?.handle(url))!
        }
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        AppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    
}

