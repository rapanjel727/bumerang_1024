//
//  ReceivingCell.swift
//  bumerang
//
//  Created by RMS on 2019/9/15.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit

class ReceivingCell: UITableViewCell {

    @IBOutlet weak var ui_imvSender: UIImageView!
    
    @IBOutlet weak var ui_viewTxt: UIView!
    @IBOutlet weak var ui_lblUsernameDate: UILabel!
    @IBOutlet weak var ui_lblMsg: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(chat:ChatRoomModel) {
        
        ui_lblUsernameDate.text = "Username, " + chat.strDate
        ui_lblMsg.text = chat.strMsg
               
        ui_viewTxt.clipsToBounds = false
        ui_viewTxt.layer.cornerRadius = 10
        ui_viewTxt.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

}
