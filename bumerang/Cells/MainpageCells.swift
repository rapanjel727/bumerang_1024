
import Foundation
import UIKit
import SDWebImage
import Photos
import iOSDropDown

class MainpageCatCell: UICollectionViewCell {
    
    @IBOutlet weak var ui_topView: UIView!
    @IBOutlet weak var ui_catagoryImg: UIImageView!
    
    @IBOutlet weak var cons_width_topView: NSLayoutConstraint!
    
    
    var entity : MainCatagoryModel! {
        didSet{
            if let img = UIImage(named: entity.catatoryImg) {
                ui_catagoryImg.image = img
            }
            
            if entity.catagoryState {
                
                ui_topView.backgroundColor = UIColor(rgb: entity!.catagoryColor!)
                
                cons_width_topView.constant = cons_width_topView.constant
            } else {
                ui_topView.backgroundColor = UIColor(rgb: 0xDFDFDF)
                cons_width_topView.constant = cons_width_topView.constant * 0.8
            }
            
            ui_topView.cornerRadius = ui_topView.frame.height / 2
            
        }
    }

    
}

class MainpageAdsCell: UICollectionViewCell {

    @IBOutlet weak var ui_imgMain: UIImageView!
    @IBOutlet weak var ui_lblDescription: UILabel!
    
    var entity : MainAdsModel! {

        didSet{
            if entity.adsImg!.starts(with: "http") {
                ui_imgMain.sd_imageIndicator = SDWebImageActivityIndicator.gray
                ui_imgMain.sd_setImage(with: URL(string: entity.adsImg!)) { (image, error, cache, urls) in
                    if (error != nil) {
                        // Failed to load image
                        self.ui_imgMain.image = UIImage(named: self.entity.placeHolerImage)
                    } else {
                        // Successful in loading image
                        self.ui_imgMain.image = image
                    }
                }
            } else {
                ui_imgMain.image = UIImage(named: entity.adsImg!)
            }
            
            ui_lblDescription.text = entity.adsTitle

        }
    }
}

class MainpageFilterCell: UICollectionViewCell {
    
    @IBOutlet weak var ui_lblFilter: UILabel!
    @IBOutlet weak var ui_dropdownFilter: DropDown!
    
    var dropDelegate : DropdownDelegate!
    
    var entity : MainFilterModel! {
        
        didSet{
            
            ui_lblFilter.text = entity.titleLbl
            ui_dropdownFilter.optionArray = entity.filterLbls
            ui_dropdownFilter.optionIds = entity.filterIDs
            ui_dropdownFilter.text = ""
            ui_dropdownFilter.isSearchEnable = false
            ui_dropdownFilter.checkMarkEnabled = false
            
            ui_dropdownFilter.didSelect{(selectedText , index , id) in
                
                self.dropDelegate.onSelect(categoryID: self.tag, dropIndex: self.ui_dropdownFilter.tag, selectedVal: selectedText)
            }
            
        }
    }
}
    
class MainpageProductCell: UICollectionViewCell {
    
    @IBOutlet weak var ui_img_main: UIImageView!
    @IBOutlet weak var ui_img_rent: UIImageView!
    @IBOutlet weak var ui_lblUsertype: UILabel!
    @IBOutlet weak var ui_lblDeposit: UILabel!
    @IBOutlet weak var ui_lblTitle: UILabel!
    @IBOutlet weak var ui_lblPrice: UILabel!
    
    var entity : MainProductModel! {
        
        didSet{
            
            if entity.image_url.starts(with: "http") {
                ui_img_main.sd_setImage(with: URL(string: entity.image_url)) { (image, error, cache, urls) in
                    if (error != nil) {
                        // Failed to load image
                        self.ui_img_main.image = UIImage(named: self.entity.placeHolerImage)
                    } else {
                        // Successful in loading image
                        self.ui_img_main.image = image
                    }
                }
                
            } else {
                ui_img_main.image = UIImage(named: entity.placeHolerImage)
            }
            
            ui_img_rent.isHidden = !entity.rental_status
            
            if entity.deposit == "Yes" { ui_lblDeposit.isHidden = false }
            else { ui_lblDeposit.isHidden = true }

            ui_lblUsertype.text = Constants.userTypeAPI[1]
            ui_lblTitle.text = entity.title
            ui_lblPrice.text = Constants.currencyUnit + "\(entity.price)/" + entity.price_type
            
        }
    }
}
protocol DropdownDelegate {
    
    func onSelect(categoryID: Int, dropIndex: Int, selectedVal: String)
}

