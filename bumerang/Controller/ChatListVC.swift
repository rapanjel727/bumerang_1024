//
//  ChatVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/12.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class ChatListVC: BaseViewController {
    
    let databaseChats = Constants.refs.databaseRoot.child("Bumerang_chat")
    var chatlistData = [ChatListModel]()
    
    @IBOutlet weak var ui_searchBar: UISearchBar!
    @IBOutlet weak var ui_chatdata_coll: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadListdata()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func loadListdata() {
        for i in 0 ..< 10 {
            
            let senderId = (i % 2 == 0 ? Defaults[.userId] : i)
            let receiverId = (i % 2 == 0 ? i : Defaults[.userId])
            
            let one = ChatListModel(senderId: senderId, receiverId: receiverId, imgName: "avatar_1", username: "user \(i)", contentStr: "hey, what are you doing now?", reqDate: "1hr ago", unreadNum: i)
            chatlistData.append(one)
        }
        
        ui_chatdata_coll.reloadData()
    }
    
    @IBAction func onClickPluse(_ sender: Any) {
        self.gotoNavigationScreen("CatagorySelectVC", direction: .fromLeft)
    }
    
    @IBAction func onClickChart(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onClickChat(_ sender: Any) {}
    
    @IBAction func onClickRend(_ sender: Any) {
        self.gotoNavigationScreen("RentHistoryVC", direction: .fromLeft)
    }
    
    @IBAction func onClickMyProfile(_ sender: Any) {
        self.gotoNavigationScreen("UserInfoVC", direction: .fromLeft)
    }
}

extension ChatListVC : UISearchBarDelegate {
    
    private func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    private func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.endEditing(true)
    }
}

extension ChatListVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let toVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatRoomVC") as! ChatRoomVC
        toVC.receiveUserId = chatlistData[indexPath.row].receiverId
        self.navigationController?.pushViewController(toVC, animated: true)
        
    }
    
}

extension ChatListVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return chatlistData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatListCell", for: indexPath) as! ChatListCell
        cell.entity = chatlistData[indexPath.row]
        
        return cell
    }
}

extension ChatListVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width
        let h : CGFloat = 100
        return CGSize(width: w, height: h)
    }
}
