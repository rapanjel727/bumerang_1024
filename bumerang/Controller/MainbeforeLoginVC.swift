//
//  MainbeforeLoginVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/5.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SwiftyJSON
import UPCarouselFlowLayout
import iOSDropDown
import FanMenu
import Macaw

class MainbeforeLoginVC: BaseViewController {
    
    @IBOutlet weak var ui_searchBar: UISearchBar!
    @IBOutlet weak var ui_collection_cat: UICollectionView!
    @IBOutlet weak var ui_collection_ads: UICollectionView!
    @IBOutlet weak var ui_collection_prod: UICollectionView!
    @IBOutlet weak var ui_collection_filter: UICollectionView!
    @IBOutlet weak var ui_view_coll_ads: UIView!
    @IBOutlet weak var ui_view_coll_filter: UIView!
    @IBOutlet weak var ui_butSign: UIButton!
    
    @IBOutlet weak var ui_butSignGroup: UIView!
    
    var catagoryData = [MainCatagoryModel]()
    var adsData     = [MainAdsModel]()
    var productData = [MainProductModel]()
    
    var allFilterData  = [[MainFilterModel]()]
    var oneFilterCell = [MainFilterModel]()
    
    var priOffsetY : CGFloat = 0
    var currentCatagory = 0
    var currentAdsPage = 0
    var adsHeight : CGFloat = 0
    
    // product filter
    var roomNum = ""
    var heating = ""
    var gender = ""
    var size = ""
    var color = ""
    var furbished = ""
    var fuel = ""
    var gear = ""
    var carType = ""
    var bedNum = ""
    var personNum = ""
    var captan = ""
    var price = ""
    var priceType = ""
    var doorNum = ""
    var deposit = ""
    
    var pageSize: CGSize {
        let layout = ui_collection_ads.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        pageSize.width += layout.minimumLineSpacing
        return pageSize
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        gotoProductAdsApi(catagoryId: 0, pageNum: 1)
        
        loadCatagoryData()
        setFilterAllData()
        
        if (Defaults[.email] == "" ) {
            
            ui_butSign.setTitle("SIGN UP", for: .normal)
            
        } else {
            
            ui_butSign.setTitle("LOG IN", for: .normal)
        }
        
        self.ui_butSignGroup.isHidden = true
        
//        self.view.sendSubviewToBack(ui_butSignGroup)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        ui_collection_cat.reloadData()
        ui_collection_cat.layoutIfNeeded()
        
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        
    }
    
    func setupLayout() {
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.ui_collection_ads.frame.width * 0.6, height: self.ui_collection_ads.frame.height)
        layout.scrollDirection = .horizontal
        layout.spacingMode = .fixed(spacing: 20)
        layout.sideItemScale = 0.9
        layout.sideItemAlpha = 1
        
        ui_collection_ads.collectionViewLayout = layout
        
    }

    func showSignupGroups(){
        UIView.animate(withDuration: 0.35, animations: {
            self.ui_butSignGroup.isHidden = !self.ui_butSignGroup.isHidden
        })
    }
    
    func setFilterAllData(){
        
        allFilterData.removeAll()
        oneFilterCell.removeAll()
        for index in 0 ..< 11 {
            
            switch index {
            case 0:
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Room number", filterLbls: RoomNumberOption().values, filterIDs: RoomNumberOption().ids, cellWidth: CGFloat(120)) )
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Heating", filterLbls: HeatingOption().values, filterIDs: HeatingOption().ids, cellWidth: CGFloat(140)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Furbished", filterLbls: BoolTypeOption().values, filterIDs: BoolTypeOption().ids, cellWidth: CGFloat(90)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Price", filterLbls: PriceFlatOption().values, filterIDs: PriceFlatOption().ids, cellWidth: CGFloat(110)))
                
            case 1:
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Fuel Type", filterLbls: FuelTypeOption().values, filterIDs: FuelTypeOption().ids, cellWidth: CGFloat(120)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Gear Type", filterLbls: GearOption().values, filterIDs: GearOption().ids, cellWidth: CGFloat(120)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Door Number", filterLbls: RoomNumberOption().values, filterIDs: RoomNumberOption().ids, cellWidth: CGFloat(110)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Car Type", filterLbls: CarTypeOption().values, filterIDs: CarTypeOption().ids, cellWidth: CGFloat(90)))
                
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Price", filterLbls: PriceCarOption().values, filterIDs: PriceCarOption().ids, cellWidth: CGFloat(100)))
            case 2:
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Bed Capacity", filterLbls: RoomNumberOption().values, filterIDs: RoomNumberOption().ids, cellWidth: CGFloat(120)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Fuel Type", filterLbls: FuelTypeOption().values, filterIDs: FuelTypeOption().ids, cellWidth: CGFloat(120)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Price", filterLbls: PriceCaravanSeaOption().values, filterIDs: PriceCaravanSeaOption().ids, cellWidth: CGFloat(100)))
            default: break
            }
            
            oneFilterCell.append(MainFilterModel(titleLbl: "Deposit", filterLbls: BoolTypeOption().values, filterIDs: BoolTypeOption().ids, cellWidth: CGFloat(70)))
            
            allFilterData.append(oneFilterCell)
            
        }
        
        oneFilterCell.removeAll()
    }
    
    @IBAction func onClickLocation(_ sender: Any) {
        showToast("ClickLocationButton", duration: 2, position: .center)
    }
    
    @IBAction func onClickSignup(_ sender: Any) {
        
        if (Defaults[.email] == "" ) {
            showSignupGroups()
        } else {
            dissmisAndGotoVC("SigninVC")
        }
    }
    
    @IBAction func onTapedStandard(_ sender: Any) {
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "SingupSetVC") as! SingupSetVC
        toVC.currentPage = 0
        
        self.present(toVC, animated: true, completion: nil)
    }
    
    @IBAction func onTapedBusiness(_ sender: Any) {
        
        self.dissmisAndGotoVC("SelCategoryBusinessVC")
        
    }
    
    
    func loadCatagoryData(){
        for i in 0 ..< Constants.cateImage.count {
            let one = MainCatagoryModel(
                catagoryId: i,
                catatoryImg: Constants.cateImage[i],
                catagoryColor: Constants.cataColor[i],
                catagoryState: false
            )
            catagoryData.append(one)
        }
        
        ui_collection_cat.reloadData()
    }
    
//    func loadAdsData(_ cataID : Int){
//        for i in 0 ..< 11 {
//            
//            //MainAdsModel(adsId: i, adsImg: "prod", adsTitle: <#T##String#>, adsDescription: <#T##String#>)
//            let one = MainAdsModel(adsId: i, adsImg: "prod",adsTitle: "", adsDescription: "advertisement\nwonderfull dress\(i)")
//            adsData.append(one)
//        }
//        
//        ui_collection_ads.reloadData()
//    }
    
    func loadFilterData(_ catagoryID: Int) {
        
        oneFilterCell.removeAll()
        oneFilterCell = allFilterData[catagoryID]
        
        ui_collection_filter.reloadData()
        ui_collection_filter.layoutIfNeeded()
    }
    
    func gotoProductAdsApi(catagoryId : Int, pageNum : Int) {
        
        self.showHUD()
        if pageNum == 1 {
            self.adsData.removeAll()
            self.productData.removeAll()
        }
        
        switch catagoryId {
        case 1:
            roomNum = ""
            heating = ""
        default:
            roomNum = ""
        }
        
        // in case dashboard
        
        if catagoryId == 0 {
            
            adsHeight = ui_collection_ads.height
            ui_view_coll_ads.height = 0
            ui_view_coll_filter.height = 0
            
            ui_view_coll_ads.isHidden = true
            ui_view_coll_filter.isHidden = true
        }
            // in case selected catagory
        else {
            ui_collection_ads.height = adsHeight
            ui_view_coll_filter.height = 50
            
            ui_view_coll_ads.isHidden = false
            ui_view_coll_filter.isHidden = false
        }
        
        ProductApiManager.getProductList(catagoryId: "\(catagoryId)", pageNum: "\(pageNum)", roomNum: roomNum, heating: heating, gender: gender, size: size, color: color, furbished: furbished, fuel: fuel, gear: gear, carType: carType, bedNum: bedNum, personNum: personNum, captan: captan, price: price, priceType: priceType, doorNum: doorNum, deposit: deposit, completion: {(isSuccess, resProducductData, resAdsData) in
            
            self.hideHUD()
            
            if (isSuccess) {
                //product data
                let productList = JSON(resProducductData!).arrayValue
                
                for one in productList {
                    let oneProduct = MainProductModel(dict: one)
                    self.productData.append(oneProduct)
                }
                
                //advantage data
                let adsList = JSON(resAdsData!).arrayValue
                for one in adsList {
                    let oneAds = MainAdsModel(dict: one)
                    self.adsData.append(oneAds)
                }
                
                for i in 0 ..< self.adsData.count {
                    self.adsData[i].adsImg = self.productData[0].placeHolerImage
                }
                
                
                if self.adsData.count == 0 {
                    self.ui_collection_ads.isHidden = true
                } else {
                    self.ui_collection_ads.isHidden = false
                }
                
                if catagoryId > 0, pageNum == 1 {
                    self.loadFilterData(catagoryId - 1)
                }
                
                
            }
            else {
                if resProducductData == nil { self.showToast(R_EN.string.CONNECT_FAIL) }
                else { self.showToast(JSON(resProducductData!).stringValue) }
            }
            
            self.ui_collection_prod.reloadData()
            self.ui_collection_ads.reloadData()
        })
    }
    
    func gotoProdauctdetailpageVC( productID : Int) {
        
        var nameVC = ""
        let catagoryNum = productData[productID].category
        
        switch  catagoryNum {
        case 1 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeDetailVC") as! HomeDetailVC
            toVC.oneProduct = productData[productID]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 2 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailVC") as! CarDetailVC
            toVC.oneProduct = productData[productID]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 3:
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CaravanDetailVC") as! CaravanDetailVC
            toVC.oneProduct = productData[productID]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 4 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "SeaVehicleDetailVC") as! SeaVehicleDetailVC
            toVC.oneProduct = productData[productID]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 5 :
            
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "ClothDetailVC") as! ClothDetailVC
            toVC.oneProduct = productData[productID]
            self.navigationController?.pushViewController(toVC, animated: true)
        case 6:
            
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "BikeDetailVC") as! BikeDetailVC
            toVC.oneProduct = productData[productID]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 7 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productID]
            toVC.navigationTitle = "Camera Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 8 :
            
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productID]
            toVC.navigationTitle = "Spore Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 9:
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productID]
            toVC.navigationTitle = "Kamp Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 10 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productID]
            toVC.navigationTitle = "Music Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 11 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productID]
            toVC.navigationTitle = "Other Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        default:
            nameVC = ""
        }
        
        if nameVC.isEmpty {
            return
        }
        
        showToast(nameVC, duration: 1, position: .center)
    }
}

extension MainbeforeLoginVC : UISearchBarDelegate {
    
    private func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    private func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        showToast("searchBarSearchButtonClicked", duration: 2, position: .center)
        searchBar.endEditing(true)
    }
}

extension MainbeforeLoginVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.ui_collection_cat {
            
            currentCatagory = indexPath.row
            
            for i in 0 ..< catagoryData.count {
                let state = (i == currentCatagory ? true : false)
                catagoryData[i].catagoryState = state
            }
            ui_collection_cat.reloadData()
            
            self.gotoProductAdsApi(catagoryId: currentCatagory + 1, pageNum: 1)
            
        }
        else if collectionView == self.ui_collection_ads {
            
            self.ui_collection_ads.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            currentAdsPage = indexPath.row
            
        } else {
            
            self.gotoProdauctdetailpageVC(productID: indexPath.row)
        }
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.ui_collection_prod {
            priOffsetY = scrollView.contentOffset.y
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.ui_collection_ads {
            let total = scrollView.contentSize.width - scrollView.bounds.width
            let offset = scrollView.contentOffset.x
            let percent = Double(offset / total)
            currentAdsPage = Int(percent * Double(self.adsData.count - 1))
            
        } else if scrollView == self.ui_collection_prod { }
    }
}

extension MainbeforeLoginVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.ui_collection_cat {return catagoryData.count}
        if collectionView == self.ui_collection_ads {return adsData.count}
        if collectionView == self.ui_collection_filter {return oneFilterCell.count}
        return productData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.ui_collection_cat {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageCatCell", for: indexPath) as! MainpageCatCell
            cell.entity = catagoryData[indexPath.row]
            
            return cell
        }
        else if collectionView == self.ui_collection_ads {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageAdsCell", for: indexPath) as! MainpageAdsCell
            cell.entity = adsData[indexPath.row]
            return cell
        }
        else if collectionView == self.ui_collection_filter {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageFilterCell", for: indexPath) as! MainpageFilterCell
            cell.entity = oneFilterCell[indexPath.row]
            cell.ui_dropdownFilter.tag = indexPath.row
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageProductCell", for: indexPath) as! MainpageProductCell
        cell.entity = productData[indexPath.row]
        return cell
    }
}

extension MainbeforeLoginVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var w : CGFloat = 0
        var h : CGFloat = 0
        
        if collectionView == self.ui_collection_cat {
            h = collectionView.frame.size.height
            w = h
        }
            
        else if collectionView == self.ui_collection_ads {
            w = collectionView.frame.size.width * 0.7
            h = collectionView.frame.size.height
        }
            
        else if collectionView == self.ui_collection_filter {
            w = oneFilterCell[indexPath.row].cellWidth
            h = collectionView.frame.size.height
        }
            
        else if collectionView == self.ui_collection_prod {
            
            w = (collectionView.frame.size.width - 10) / 2
            h = w * 1.1
        }
        
        return CGSize(width: w, height: h)
    }
}
