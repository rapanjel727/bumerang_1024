

import UIKit
import SwiftyUserDefaults

class SplashVC: BaseViewController {
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
    
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
//
//            let toVC = self.storyboard?.instantiateViewController( withIdentifier: "MainVC")
//
//            self.present(toVC!, animated: true, completion: nil)
//        })
        
    }
    
    @IBAction func omClickStart(_ sender: Any) {
        
        if Defaults[.rememberState] == true {
            self.dissmisAndGotoVC("MainpageAfterNav")
            
        } else if Defaults[.registerState] == true {
            
            self.dissmisAndGotoVC("MainbeforeLoginNav")
            
        } else {
            self.dissmisAndGotoVC("IntroductionVC")
        }
    }
}
