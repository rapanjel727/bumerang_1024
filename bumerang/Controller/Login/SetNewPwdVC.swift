//
//  SetNewPwdVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/9.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class SetNewPwdVC: BaseViewController {

    @IBOutlet weak var ui_viewMain: UIView!
    @IBOutlet weak var ui_newPwd: CustomTextField!
    @IBOutlet weak var ui_confPwd: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.hideMe(sender:))))
        
        ui_viewMain.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    @IBAction func onClickChagePwd(_ sender: Any) {
        
        let newPwd = ui_newPwd.text!
        let confPwd = ui_confPwd.text!
        
        if newPwd.isEmpty {
            showToast(R_EN.string.INPUT_NEW_PWD, duration: 2, position: .center)
            return
        } else if confPwd.isEmpty {
            showToast(R_EN.string.INPUT_CONFIRM_PWD, duration: 2, position: .center)
            return
        } else if newPwd != confPwd {
            showToast(R_EN.string.INVAILD_PWD, duration: 2, position: .center)
            return
        } else {
            gotoSetNewPwdAPI(newPwd)
            
        }
    }
    
    func gotoSetNewPwdAPI(_ newPwd : String) {
        
        //call api
        
        //gogto sign in if  succese
        self.hideThisVC()
    }
    
    @objc func hideMe(sender: UITapGestureRecognizer){
        
        self.hideThisVC()
    }
    
    func hideThisVC(){
        if signinVC != nil {
            
            ui_newPwd.text = ""
            ui_confPwd.text = ""
            
            ui_newPwd.endEditing(true)
            ui_confPwd.endEditing(true)
            
            signinVC.hideSetNewPwdVC()
        } else {
            print("error")
        }
    }
}
