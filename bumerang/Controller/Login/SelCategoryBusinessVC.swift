//
//  SelCategoryBusinessVC.swift
//  bumerang
//
//  Created by RMS on 2019/10/8.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit

class SelCategoryBusinessVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onTapedCategory(_ sender: UIButton) {
        
        let str = Constants.categoryName[sender.tag] + " Product selected!"
        
        let alert = UIAlertController(title: str, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: R_EN.string.CANCEL, style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: R_EN.string.OK, style: .default, handler : {(action) -> Void in
            
            let toVC = self.storyboard?.instantiateViewController( withIdentifier: "SelectMemberShipVC") as! SelectMemberShipVC
            toVC.selectCategory = sender.tag
            toVC.modalPresentationStyle = .fullScreen
            
            self.present(toVC, animated: true, completion: nil)
            
        }))
        
        DispatchQueue.main.async(execute:  {
            self.present(alert, animated: true, completion: nil)
        })
        
        
    }
    
}
