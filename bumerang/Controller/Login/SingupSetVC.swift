//
//  SingupVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/4.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout

class SingupSetVC: BaseViewController {

    @IBOutlet weak var ui_collection: UICollectionView!
    
    var selectedCategory = -1
    var selectedMembership = -1
    
    var currentPage = 0
    var dataCollection = [IntroModel]()
    
    let imgNames = ["users_standard", "ic_house"]
    let topicStr = [R_EN.string.USER_REG_STANDARD, R_EN.string.USER_REG_BUSINESS]
    let contentStr = [
        "user_reg_standard\nadafasdsfdgs\nadafasd",
        "user_reg_business\nadafasdsfdgs\nadafasdsfdgs\nadafasd"
    ]
    
    var pageSize: CGSize {
        let layout = ui_collection.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        pageSize.width += layout.minimumLineSpacing
        return pageSize
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        for i in 0 ..< imgNames.count {
            let one = IntroModel(imgName: imgNames[i], topicStr: topicStr[i], contentStr: contentStr[i])
            dataCollection.append(one)
        }
        
        self.ui_collection.scrollToItem(at: IndexPath(row: currentPage, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupLayout()
    }
    
    func setupLayout() {
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.ui_collection.frame.width * 0.8, height: self.ui_collection.bounds.height)
        layout.scrollDirection = .horizontal
        layout.spacingMode = .fixed(spacing: 20)
        layout.sideItemScale = 0.9
        layout.sideItemAlpha = 1
        
        ui_collection.collectionViewLayout = layout
    }
    
    @IBAction func onClickSignup(_ sender: Any) {
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "SignupVC") as! SignupVC
        toVC.curUserType = self.currentPage
        toVC.curCategory = self.selectedCategory
        toVC.curMembership = self.selectedMembership
        toVC.modalPresentationStyle = .fullScreen
        
        self.present(toVC, animated: true, completion: nil)
    }
    
    @IBAction func onClickLogin(_ sender: Any) {
    
//        self.gotoNavigationScreen("SigninVC", direction: .fromBottom)
        self.dissmisAndGotoVC("SigninVC")
    }
    
    
}

extension SingupSetVC : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SignupSliderCell", for: indexPath) as! SignupSliderCell
        cell.entity = dataCollection[indexPath.row]
        return cell
    }
}

extension SingupSetVC : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.ui_collection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        currentPage = indexPath.row
    }
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = ui_collection.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }
}

extension SingupSetVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let w = collectionView.frame.size.width * 0.8
        let h = collectionView.frame.size.height

        return CGSize(width: w, height: h)
    }
}
