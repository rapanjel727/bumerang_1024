
import UIKit
import SwiftyJSON
import SwiftyUserDefaults

import FacebookCore
import FBSDKLoginKit
import Firebase
import GoogleSignIn
import FirebaseAuth

class SignupVC: BaseViewController {

    @IBOutlet weak var lnameTxt : CustomTextField!
    @IBOutlet weak var fnameTxt : CustomTextField!
    @IBOutlet weak var emailTxt : CustomTextField!
    @IBOutlet weak var pwdTxt : CustomTextField!
    @IBOutlet weak var confPwdTxt : CustomTextField!
    @IBOutlet weak var ui_lblUserType: UILabel!
    
    var curUserType = 0
    var curCategory = 0
    var curMembership = -1
    
    let userTypeColor = ["primary", "redColor"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        let userType = Constants.userType[0]
        
        ui_lblUserType.text = "You are \(userType) user in Bumerang"
        ui_lblUserType.textColor = UIColor(named: userTypeColor[curUserType])
        
        
        GIDSignIn.sharedInstance()?.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onClickSignup(_ sender: Any) {
        
        let lname = lnameTxt.text!.trimmed
        let fname = fnameTxt.text!.trimmed
        let email = emailTxt.text!.trimmed
        let pwd = pwdTxt.text!.trimmed
        let confPwd = confPwdTxt.text!.trimmed
        
        if lname.isEmpty {
            showToast(R_EN.string.INPUT_LNAME, duration: 2, position: .center)
            return
        }
        
        if fname.isEmpty {
            showToast(R_EN.string.INPUT_FNAME, duration: 2, position: .center)
            return
        }
        
        if email.isEmpty {
            showToast(R_EN.string.INPUT_EMAIL, duration: 2, position: .center)
            return
        } else if !isValidEmail(testStr: email) {
            showToast(R_EN.string.INVALID_EMAIL, duration: 2, position: .center)
            return
        }
        
        if pwd.isEmpty {
            showToast(R_EN.string.INPUT_PWD, duration: 2, position: .center)
            return
        }
        
        if confPwd.isEmpty {
            showToast(R_EN.string.INPUT_CONFIRM_PWD, duration: 2, position: .center)
            return
        }
        
        if pwd != confPwd {
            showToast(pwd + ", " + confPwd, duration: 2, position: .top)
            showToast(R_EN.string.INVAILD_PWD, duration: 2, position: .center)
            return
        }
        let userTypeApi = Constants.userTypeAPI[curUserType]
        let authType = Constants.mailType[0]
        gotoSignupAPI(fname: fname, lname: lname, email: email, pwd: pwd, user_type: userTypeApi, auth_type: authType)
        
    }
    
    @IBAction func onClickFBSignup(_ sender: Any) {
            
//        if let accessToken = AccessToken.current {
//            print(accessToken)
//            getUserData()
//        } else {
        
            let loginManager = LoginManager()
            
            loginManager.logIn(permissions: ["public_profile", "email"], from: self) { [weak self](result, error) in
                
                if error != nil {
                    print("Login failed")
                } else {
                    if result!.isCancelled { print("login canceled") }
                    else { self!.getFaceBookUserData() }
                }
            }
//        }
    }
    
        
    func getFaceBookUserData() {
        
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, first_name, last_name"])) { (httpResponse, result, error) in

//            print("graph AP I result: ", result)
            let jsonResult = JSON(result)
            let fname = jsonResult["first_name"].stringValue
            let lname = jsonResult["last_name"].stringValue
            let email = jsonResult["email"].stringValue
            
            let id = jsonResult["id"].intValue
            let profile_photo = "http://graph.facebook.com/\(id)/picture?type=large"

            
            print(id, " : ", email)
            print(fname, " : ", lname)
            print(profile_photo)
            
            self.gotoSignupAPI(fname: fname, lname: lname, email: email, pwd: "", user_type: Constants.userTypeAPI[self.curUserType], auth_type: Constants.mailType[1])
        }

        connection.start()
    }
    
    @IBAction func onTappedGoogleSignup(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func onTapedTerms(_ sender: Any) {
        
        self.GotoNewVC("TermsVC")
    }
    
    @IBAction func onClickSignin(_ sender: Any) {
//        self.gotoNavigationScreen("SigninVC", direction: .fromBottom)
        self.dissmisAndGotoVC("SigninVC")
        
//        self.GotoNewVC("SigninVC")
    }
    
    func gotoSignupAPI(fname: String, lname: String, email: String, pwd: String, user_type: String, auth_type: String) {
        
        showHUD()
        UserApiManager.signup(fname: fname, lname: lname, email: email, pwd: pwd, user_type: user_type, auth_type: auth_type) { (isSuccess, data) in
            
            self.hideHUD()
            
            if isSuccess {
                
                //let userInfo = JSON(data!).stringValue
                g_user = UserModel(userinfo: JSON(data!))
                g_user.pwd = pwd
                
                Defaults[.firstname] = g_user.first_name
                Defaults[.lastname] = g_user.last_name
                Defaults[.userId] = g_user.userId
                Defaults[.email] = g_user.email
                Defaults[.pwd] = pwd
                Defaults[.registerState] = true
                Defaults[.userType] = g_user.user_type
                
                self.gotoMainAfterVC()
                
            } else {
                
                if data == nil {
                    self.showToast(R_EN.string.CONNECT_FAIL)

                } else {
                    self.showToast(JSON(data!).stringValue)
                }
            }
        }
        
    }
}

extension SignupVC: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
            
            if let error = error {
                print(error.localizedDescription)
                return
            }
    //        guard  let auth = user.authentication else {
    //            return
    //        }
            
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            
            print(userId, idToken, fullName, givenName, familyName, email)
            
        gotoSignupAPI(fname: fullName!, lname: familyName!, email: email!, pwd: "", user_type: Constants.userTypeAPI[self.curUserType], auth_type: Constants.mailType[2])
            
    //        let credentials = GoogleAuthProvider.credential(withIDToken: auth.idToken, accessToken: auth.accessToken)
        
    //        Auth.auth().signIn(with: credentials) { (authResult, error) in
    //
    //            if let error = error {
    //                print(error.localizedDescription)
    //                return
    //            }
    //
    //            // TODO: ----
    //        }
            
        }
        

}
