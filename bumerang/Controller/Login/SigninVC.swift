//
//  SigninVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/5.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import GDCheckbox
import SwiftyUserDefaults
import SwiftyJSON

import FacebookCore
import FBSDKLoginKit
import Firebase
import GoogleSignIn

var signinVC : SigninVC!

class SigninVC: BaseViewController {

    var recoveryEmailVC : RecoveryEmailVC!
    var verifyCodeVC : VerifyCodeVC!
    var setNewPwdVC : SetNewPwdVC!
    
    @IBOutlet weak var ui_blackView: UIView!
    @IBOutlet weak var ui_emailTxt: CustomTextField!
    @IBOutlet weak var ui_pwdTxt: CustomTextField!
    @IBOutlet weak var ui_robotCheck: GDCheckbox!
    
    @IBOutlet weak var cons_topView_top: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ui_emailTxt.text = Defaults[.email]
        ui_pwdTxt.text = Defaults[.pwd]
                
        cons_topView_top.constant = UIScreen.main.bounds.height / 15
        
        signinVC = self
        
        ui_blackView.isHidden = true
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        recoveryEmailVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecoveryEmailVC") as! RecoveryEmailVC)
        recoveryEmailVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        verifyCodeVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyCodeVC") as! VerifyCodeVC)
        verifyCodeVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        setNewPwdVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetNewPwdVC") as! SetNewPwdVC)
        setNewPwdVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
    }
    
    @IBAction func onClickSignup(_ sender: Any) {
//        self.gotoNavigationScreen("SingupSetVC", direction: .fromBottom)
        self.dissmisAndGotoVC("SingupSetVC")
//        self.GotoNewVC("SingupSetVC")
    }
    
    @IBAction func onClickSignin(_ sender: Any) {
        
        let emailTxt = ui_emailTxt.text!
        let pwdTxt = ui_pwdTxt.text!
        
        if emailTxt.isEmpty {
            showToast(R_EN.string.INPUT_EMAIL, duration: 2, position: .center)
            return
        }
        
        if !isValidEmail(testStr: emailTxt) {
            showToast(R_EN.string.INVALID_EMAIL, duration: 2, position: .center)
            return
        }
            
        if pwdTxt.isEmpty {
            showToast(R_EN.string.INPUT_CONFIRM_PWD, duration: 2, position: .center)
            return
        } else {
            
            if Defaults[.rememberState] == true {
                self.gotoMainAfterVC()
            } else {
                
                let authType = Constants.mailType[0]
                gotoSigninAPI(email: emailTxt, pwd: pwdTxt, auth_type : authType)
            }
        }
    }
    
    @IBAction func onTapedGoogleSignIn(_ sender: Any) {
        
       GIDSignIn.sharedInstance()?.presentingViewController = self
       GIDSignIn.sharedInstance()?.signIn()
    }
    
    func setRememberState() {
        Defaults[.rememberState] = ui_robotCheck.isOn
    }
    
    func gotoSigninAPI(email : String, pwd : String, auth_type : String) {

        showHUD()
        // call api
        UserApiManager.login(email: email, pwd: pwd, auth_type : auth_type){ (isSuccess, data) in
            
            self.hideHUD()
            
            if isSuccess {
                
                g_user = UserModel(userinfo: JSON(data!))
                g_user.pwd = pwd
                
                Defaults[.firstname] = g_user.first_name
                Defaults[.lastname] = g_user.last_name
                Defaults[.userId] = g_user.userId
                Defaults[.email] = g_user.email
                Defaults[.pwd] = pwd
                Defaults[.registerState] = true
                Defaults[.userType] = g_user.user_type
                
                self.setRememberState()
                self.gotoMainAfterVC()
                
            } else {
                
                if data == nil {
                    self.showToast(R_EN.string.CONNECT_FAIL)
                } else {
                    self.showToast(JSON(data!).stringValue)
                }
            }
        }
        
    }
    
    @IBAction func onCheckBoxPress(_ sender: GDCheckbox) {
        ui_robotCheck.containerColor = UIColor(named: "black")!
    }
    
    // chage CheckSbox tate
    @IBAction func onClickRobot(_ sender: Any) {
        
        ui_robotCheck.isOn = !ui_robotCheck.isOn
        ui_robotCheck.containerColor = UIColor(named: "black")!
    }
    
    @IBAction func onClickForgot(_ sender: Any) {
        ui_blackView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            
            let sub1VC = self.recoveryEmailVC
            sub1VC!.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(sub1VC!.view)
            
            self.addChild(sub1VC!)
            
        })
    }
    
    @IBAction func onClickFBSignup(_ sender: Any) {
                
//        if let accessToken = AccessToken.current {
//            print(accessToken)
//            getUserData()
//        } else {
        
            let loginManager = LoginManager()
            
            loginManager.logIn(permissions: ["public_profile", "email"], from: self) { [weak self](result, error) in
                
                if error != nil {
                    print("Login failed")
                } else {
                    if result!.isCancelled { print("login canceled") }
                    else { self!.getFaceBookUserData() }
                }
            }
//        }
    }
    
    func getFaceBookUserData() {
        
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, first_name, last_name"])) { (httpResponse, result, error) in

//            print("graph AP I result: ", result)
            let jsonResult = JSON(result)
            let fname = jsonResult["first_name"].stringValue
            let lname = jsonResult["last_name"].stringValue
            let email = jsonResult["email"].stringValue
            
            let id = jsonResult["id"].intValue
            let profile_photo = "http://graph.facebook.com/\(id)/picture?type=large"

            
            print(id, " : ", email)
            print(fname, " : ", lname)
            print(profile_photo)
            
            self.gotoSigninAPI(email : email, pwd : "", auth_type : Constants.mailType[1])
            
        }

        connection.start()
    }
    
    func gotoVerifyCodeVC() {
        
        hideRecoveryEmailVC(false)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            let sub2VC = self.verifyCodeVC
            sub2VC!.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(sub2VC!.view)
            
            self.addChild(sub2VC!)
        })
        
    }
    
    func gotoSetPwdVC() {
        
        hideVerifyCodeVC(false)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            let subVC = self.setNewPwdVC
            subVC!.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(subVC!.view)
            
            self.addChild(subVC!)
        })
        
    }
    
    func hideSetNewPwdVC() {
        UIView.animate(withDuration: 0.2, animations: {
            self.setNewPwdVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.setNewPwdVC.view.willRemoveSubview(self.setNewPwdVC.view)
        })
        ui_blackView.isHidden = true
        
    }
    
    func hideRecoveryEmailVC(_ blackViewState: Bool){
        
        self.recoveryEmailVC.view.endEditing(true)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.recoveryEmailVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.recoveryEmailVC.view.willRemoveSubview(self.recoveryEmailVC.view)
        })
        
        ui_blackView.isHidden = blackViewState
        
    }
    
    func hideVerifyCodeVC(_ blackViewState: Bool){
        UIView.animate(withDuration: 0.2, animations: {
            self.verifyCodeVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.verifyCodeVC.view.willRemoveSubview(self.verifyCodeVC.view)
        })
        
        ui_blackView.isHidden = blackViewState
    }
    
}


extension SigninVC: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
            
        if let error = error {
            print(error.localizedDescription)
            return
        }
//        guard  let auth = user.authentication else {
//            return
//        }
        
        // Perform any operations on signed in user here.
        //let userId = user.userID                  // For client-side use only!
        //let idToken = user.authentication.idToken // Safe to send to the server
        //let fullName = user.profile.name
        //let givenName = user.profile.givenName
        //let familyName = user.profile.familyName
        let email : String = user.profile.email!
        
        
        print(email)
            
        gotoSigninAPI(email : email, pwd : "", auth_type : Constants.mailType[2])
        
//        let credentials = GoogleAuthProvider.credential(withIDToken: auth.idToken, accessToken: auth.accessToken)
    
//        Auth.auth().signIn(with: credentials) { (authResult, error) in
//
//            if let error = error {
//                print(error.localizedDescription)
//                return
//            }
//
//            // TODO: ----
//        }
        
    }
        

}
