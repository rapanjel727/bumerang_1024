//
//  VerifyEmailVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/8.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit

class VerifyCodeVC: BaseViewController {

    @IBOutlet weak var ui_viewMain: UIView!
    @IBOutlet weak var ui_codeTxt: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.hideMe(sender:))))
        
        ui_viewMain.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    @IBAction func onClickVerify(_ sender: Any) {
        
        let codeTxt = ui_codeTxt.text!
        if codeTxt.isEmpty {
            showToast(R_EN.string.ENTER_VERIFY_CODE, duration: 2, position: .center)
            return
        }
        
        if signinVC != nil {
            ui_codeTxt.text = ""
            signinVC.gotoSetPwdVC()
        } else {
            print("error")
        }
    }
    
    @objc func hideMe(sender: UITapGestureRecognizer){
        
        if signinVC != nil {
            ui_codeTxt.text = ""
            ui_codeTxt.endEditing(true)
            signinVC.hideVerifyCodeVC(true)
        } else {
            print("error")
        }
    }

}
