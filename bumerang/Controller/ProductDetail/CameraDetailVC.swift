//
//  CameraDetailVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/24.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class CameraDetailVC: BaseViewController {

    var oneProduct : MainProductModel? = nil
    var navigationTitle = ""
    
    @IBOutlet weak var ui_navigationbarTitle: UINavigationItem!
    
    @IBOutlet weak var ui_lblRentState: UILabel!
    @IBOutlet weak var ui_imgRentState: UIImageView!
    @IBOutlet weak var ui_imgProduct: UIImageView!
    @IBOutlet weak var ui_txtTIlte: UILabel!

    @IBOutlet weak var ui_txtPrice: UILabel!
    @IBOutlet weak var ui_txvDescription: UITextView!
    @IBOutlet weak var ui_viewRent: UIView!
    @IBOutlet weak var ui_avatarView: UIView!
    @IBOutlet weak var ui_imgAvatar: UIImageView!
    @IBOutlet weak var ui_lblBUsername: UILabel!
    @IBOutlet weak var ui_lblRatingval: UILabel!
    @IBOutlet weak var ui_viewRaing: UIView!

    @IBOutlet weak var ui_imgEmail: UIImageView!
    @IBOutlet weak var ui_imgPhone: UIImageView!
    @IBOutlet weak var ui_imgGmail: UIImageView!
    @IBOutlet weak var ui_imgFace: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let oneProduct = oneProduct else {
            return
        }
        
        self.loadDitailData(oneProduct)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        ui_viewRent.cornerRadius = ui_viewRent.bounds.height/2
        ui_avatarView.cornerRadius = ui_avatarView.bounds.width/2
    }
    
    func loadDitailData(_ oneData : MainProductModel) {
            
        if oneData.image_url.starts(with: "http") {
            ui_imgProduct.sd_setImage(with: URL(string: oneData.image_url))
        }
        else {
            ui_imgProduct.image = UIImage(named: oneData.placeHolerImage)
        }
        
        ui_imgRentState.isHidden = !oneData.rental_status
        ui_txtTIlte.text = oneData.title
        ui_txtPrice.text = "Price : \(String(oneData.price))/\(String(oneData.price_type))"
        ui_txvDescription.text = oneData.description

        ui_lblBUsername.text = oneData.userinfo_fname + " " + oneData.userinfo_lname
        ui_lblRatingval.text = String(oneData.userinfo_rating)
        
        ui_imgEmail.image = UIImage(named: "ic_mail_" + (oneData.userinfo_mailState ? "blue" : "grey"))
        ui_imgPhone.image = UIImage(named: "ic_phone_" + (oneData.userinfo_mailState ? "blue" : "grey"))
        
        ui_imgGmail.image = UIImage(named: "ic_google_" + (oneData.userinfo_mailState ? "verified" : "unverified"))
        
        ui_imgFace.image = UIImage(named: "ic_facebook_" + (oneData.userinfo_mailState ? "verified" : "unverified"))
    }
    
    @IBAction func onClickChat(_ sender: Any) {
        
        if Defaults[.userId] == oneProduct?.owner_id {
            
            showToast(R_EN.string.CHAT_REQUEST_FAIL_USER, duration: 2, position: .center)
            return
        } else if Defaults[.registerState] == false {
            
            showToast(R_EN.string.CHAT_REQUEST_FAIL_LOGIN, duration: 2, position: .center)
            return
        }
                
        let toVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatRoomVC") as! ChatRoomVC
        toVC.receiveUserId = oneProduct!.owner_id
        self.navigationController?.pushViewController(toVC, animated: true)
    }
    
    @IBAction func onClickRent(_ sender: Any) {
        
        if Defaults[.userId] == oneProduct?.owner_id {
            showToast(R_EN.string.RENT_REQUEST_FAIL_USER, duration: 2, position: .center)
            return
        } else if Defaults[.registerState] == false {
            
            showToast(R_EN.string.RENT_REQUEST_FAIL_LOGIN, duration: 2, position: .center)
            return
        }
        
        let toVC = self.storyboard?.instantiateViewController(withIdentifier: "RentPorductVC") as! RentPorductVC
        toVC.oneProduct = self.oneProduct
        self.navigationController?.pushViewController(toVC, animated: true)
    }
    
}

