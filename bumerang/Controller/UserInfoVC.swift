//
//  UserProfileVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/13.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit

class UserInfoVC: BaseViewController {

    @IBOutlet weak var ui_avatarView: UIView!
    @IBOutlet weak var ui_avatarImg: UIImageView!
    
    @IBOutlet weak var ui_productLbl: UILabel!
    @IBOutlet weak var ui_lblReviewnum: UILabel!
    @IBOutlet weak var ui_product_coll: UICollectionView!
    @IBOutlet weak var ui_review_coll: UICollectionView!
    @IBOutlet weak var ui_chat_coll: DynamicHeightCollectionView!
    
    var productData = [MainProductModel]()
    var reviewData = [UserReviewModel]()
    var chatData = [UserChatModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupMenuButton()
        loadProductData()
        loadReviewData()
//        loadChatData()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        ui_avatarView.cornerRadius = ui_avatarView.height / 2
        ui_avatarImg.cornerRadius = ui_avatarImg.height / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    func loadProductData(){
        for i in 0 ..< 11 {
            let medal = (i % 2 == 0 ? false: true)
            let top1 = (i % 3 == 0 ? "top1": "")
            let top2 = (i % 3 == 1 ? "temp1": "temp1")
            let mid = (i % 2 == 0 ? "midTemp1": "midTemp2")
            let bottom : Float = i % 2 == 0 ? 50 : 100
            let dateType = "day"
            
            let one = MainProductModel(category: 1, productId: i, productImg: "prod", rentState: medal, lblTop1: top1, lblTop2: top2, lblMid: mid, price: bottom,dateType: dateType)
            
            productData.append(one)
        }
        
        ui_productLbl.text = "Product(\(productData.count))"
        
        ui_product_coll.reloadData()
        ui_product_coll.layoutIfNeeded()
    }
    
    func loadReviewData(){
        for i in 0 ..< 14 {
            
            let one = UserReviewModel(reviewId: i, avatarImg: "ic_avatar", producTitle: "Product Title", reveiwVal: Float(i % 5), reviewStr: "Great customer, he had always kept promise", userName: "JaingYin Ji", reviewDate: "10/21/2019")
            
            reviewData.append(one)
        }
        
        ui_lblReviewnum.text = "Review(\(reviewData.count))"
        ui_review_coll.reloadData()
        //ui_review_coll.layoutIfNeeded()
    }
    
    func loadChatData(){
        for _ in 0 ..< 10{
            
            let one = UserChatModel(reportDate: "20/10/2019", reportContent: "Hi, Yin Ji\nI receive your file", receiveState: 1)
            
            chatData.append(one)
        }
        
        ui_chat_coll.reloadData()
        ui_chat_coll.layoutIfNeeded()
    }
    
    
    func setupMenuButton() {
  
        //right button
        let menuRightBtn = UIButton(type: .custom)
        menuRightBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuRightBtn.setImage(UIImage(named:"ic_setting"), for: .normal)
        menuRightBtn.addTarget(self, action: #selector(rightbuttonPressed), for: .touchUpInside)
        
        let menuBarItem = UIBarButtonItem(customView: menuRightBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight?.isActive = true
        self.navigationItem.rightBarButtonItem = menuBarItem
    }
    
    @objc func rightbuttonPressed() {
        self.gotoNavigationScreen("UserProfileVC", direction: .fromRight)
    }
    
    @IBAction func leftbuttonPressed() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}


extension UserInfoVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.ui_product_coll {
            
        }
//        else {
//
//            showToast("cat: \(curCatagory), select: \(indexPath.row)", duration: 1, position: .top)
//            self.gotoProdauctdetailpageVC(catagoryNum: curCatagory, productID: indexPath.row)
//        }
    }
    
}

extension UserInfoVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.ui_product_coll {return productData.count}
        if collectionView == self.ui_review_coll {return reviewData.count}
        return productData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.ui_product_coll {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserinfoProductCell", for: indexPath) as! UserinfoProductCell
            cell.entity = productData[indexPath.row]
            
            return cell
        }
            
        else if collectionView == self.ui_review_coll {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserReviewCell", for: indexPath) as! UserReviewCell
            cell.entity = reviewData[indexPath.row]

            return cell
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageProductCell", for: indexPath) as! UserinfoProductCell
        cell.entity = productData[indexPath.row]
        return cell
    }
    
}

extension UserInfoVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var w : CGFloat = 0
        var h : CGFloat = 0
        if collectionView == self.ui_product_coll {
            w = (collectionView.frame.size.width - 10) / 2
            h = collectionView.frame.size.height / 2 - 5
        }
        
        if collectionView == self.ui_review_coll {
            
            w = collectionView.frame.size.width
            h = collectionView.frame.size.height / 2
            
            //            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("MainpageProductCell", forIndexPath: indexPath) as UICollectionViewCell
            //            configureCell(cell, item: items[indexPath.row])
            //
            //            cell.contentView.setNeedsLayout()
            //            cell.contentView.layoutIfNeeded()
            //
            //            return cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        }
        
        return CGSize(width: w, height: h)
    }
}
