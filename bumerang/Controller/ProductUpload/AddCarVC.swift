//
//  AddCarVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/10.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import iOSDropDown
import GDCheckbox
import MapKit
import Photos
import DKImagePickerController
import DKCamera
import SwiftyJSON
import SwiftyUserDefaults
import CoreLocation

enum MapType: NSInteger {
    case StandardMap = 0
    case SatelliteMap = 1
    case HybridMap = 2
}

class AddCarVC: BaseViewController {

    var selectedPeriod = 0
    var selectedFuel = ""
    var selectedGear = ""
    var selectedCarType = ""
    var imageSetName = ""
    var depositState = ""
    var lan = 0
    var long = 0
    
    var locationManager = CLLocationManager.init()

    
    @IBOutlet weak var ui_titleTxt: UITextField!
    @IBOutlet weak var ui_fuelDropDown: DropDown!
    @IBOutlet weak var ui_gearDropDown: DropDown!
    @IBOutlet weak var ui_doorTxt: UITextField!
    @IBOutlet weak var ui_cartypeDropDown: DropDown!
    @IBOutlet weak var ui_depositDropDown: DropDown!
    
    @IBOutlet weak var ui_priceTxt: UITextField!
    @IBOutlet weak var ui_dayRadio: GDCheckbox!
    @IBOutlet weak var ui_weekRadio: GDCheckbox!
    @IBOutlet weak var ui_monthRadio: GDCheckbox!
    @IBOutlet weak var ui_descriptionTxtView: UITextView!
    @IBOutlet weak var ui_uploadImg: UIImageView!
    @IBOutlet weak var ui_addrLbl: UITextField!
    @IBOutlet weak var ui_mapView: MKMapView!
    
    @IBOutlet weak var ui_uploadImgBut: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        ui_mapView.isZoomEnabled = true
        
        let fuelTypeOption = FuelTypeOption()
        ui_fuelDropDown.optionArray = fuelTypeOption.values
        ui_fuelDropDown.optionIds = fuelTypeOption.ids
        ui_fuelDropDown.checkMarkEnabled = false
        ui_fuelDropDown.didSelect{(selectedText , index , id) in
            self.selectedFuel = selectedText
        }
        
        let gearOption = GearOption()
        ui_gearDropDown.optionArray = gearOption.values
        ui_gearDropDown.optionIds = gearOption.ids
        ui_gearDropDown.checkMarkEnabled = false
        ui_gearDropDown.didSelect{(selectedText , index , id) in
            self.selectedGear = selectedText
        }
        
        let carOption = CarTypeOption()
        ui_cartypeDropDown.optionArray = carOption.values
        ui_cartypeDropDown.optionIds = carOption.ids
        ui_cartypeDropDown.checkMarkEnabled = false
        ui_cartypeDropDown.didSelect{(selectedText , index , id) in
            self.selectedCarType = selectedText
        }
        
        let depositOption = BoolTypeOption()
        ui_depositDropDown.optionArray = depositOption.values
        ui_depositDropDown.optionIds = depositOption.ids
        ui_depositDropDown.checkMarkEnabled = false
        ui_depositDropDown.didSelect{(selectedText , index , id) in
            self.depositState = selectedText
        }
        
    }
    
    @IBAction func onCheckBoxPress(_ sender: GDCheckbox) {
        
        getPeriodVal(sender.tag)
    }
    
    @IBAction func onClickDaily(_ sender: Any) {
        ui_dayRadio.isOn = true
        getPeriodVal(1)
    }
    
    @IBAction func onClickWeek(_ sender: Any) {
        ui_weekRadio.isOn = true
        getPeriodVal(2)
    }
    
    @IBAction func onClickMonth(_ sender: Any) {
        ui_monthRadio.isOn = true
        getPeriodVal(3)
    }
    
    func getPeriodVal(_ val : Int) {
        
        selectedPeriod = val
        
        if val == 1 {
            ui_weekRadio.isOn = false
            ui_monthRadio.isOn = false
        } else if val == 2 {
            ui_dayRadio.isOn = false
            ui_monthRadio.isOn = false
        } else if val == 3 {
            ui_dayRadio.isOn = false
            ui_weekRadio.isOn = false
        }
    }
    
    @IBAction func onClickUploadImg(_ sender: UIButton) {
        
        if self.ui_uploadImg.image == nil {
            self.selectImage()
            
        } else {
            let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_DELECT_IMAGE, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: R_EN.string.CANCEL, style: .default, handler: nil))
            
            alert.addAction(UIAlertAction(title: R_EN.string.OK, style: .default, handler : {(action) -> Void in
                self.ui_uploadImg.image = nil
                sender.setTitle(R_EN.string.SELECT_UPLOAD_IMAGAE, for: .normal)
            }))
            
            DispatchQueue.main.async(execute:  {
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    func selectImage(){
        
        let galleryAction = UIAlertAction(title: "from gallery", style: .destructive) { (action) in
            self.openGallery()
        }
        let cameraAction = UIAlertAction(title: "from camera", style: .destructive) { (action) in
            self.openCamera()
        }
        let cancelAction = UIAlertAction(title:R_EN.string.CANCEL, style: .cancel, handler : nil)
        
        // Create and configure the alert controller.
        let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_IMAGE_SETMODE, preferredStyle: .actionSheet)
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)

    }
    
    func openGallery() {
        let pickerController = DKImagePickerController()
        pickerController.assetType = .allAssets
        pickerController.allowSwipeToSelect = true
        pickerController.sourceType = .photo
        pickerController.singleSelect = true
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.didSelectAssets = {(assets: [DKAsset]) in self.onSelectAssets(assets: assets)
        }
        
        present(pickerController, animated: true, completion : nil)
    }
    
    func openCamera(){
        
        let camera = DKCamera()
        camera.allowsRotate = true
        camera.showsCameraControls = true
        camera.defaultCaptureDevice = DKCameraDeviceSourceType.rear
        
        camera.didCancel = {self.dismiss(animated: true, completion: nil)}
        camera.didFinishCapturingImage = { (image: UIImage?, metadata: [AnyHashable : Any]?) in
            self.dismiss(animated: true, completion: nil)
            
            if let img = image {
                self.ui_uploadImg.image = img
                self.ui_uploadImgBut.setTitle("", for: .normal)
            }
        }
        
        present(camera, animated: true, completion: nil)
    }
    
    func onSelectAssets(assets : [DKAsset]) {
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    if let img = image {
                        self.ui_uploadImg.image = img
                        self.ui_uploadImgBut.setTitle("", for: .normal)
                    }
                })
            }
        }
        else {}
    }
    
    @IBAction func onClickUpload(_ sender: Any) {
        
        var showStr = ""
        
        let titleTxt = ui_titleTxt.text!.trimmed
        let doorNum = ui_doorTxt.text!.trimmed
        let priceTxt = ui_priceTxt.text!.trimmed
        let desciptTxt = ui_descriptionTxtView.text!.trimmed
        let addrTxt = ui_addrLbl.text!.trimmed
        let lat = "\(ui_mapView.centerCoordinate.latitude)"
        let lng = "\(ui_mapView.centerCoordinate.longitude)"
        let zipCode = "123456"
        
        if titleTxt.isEmpty {
            showStr = R_EN.string.ENTER_PRODUCT_TITLE
        } else if selectedFuel.isEmpty {
            showStr = R_EN.string.SELECT_FUEL
        }
        else if selectedGear.isEmpty {
            showStr = R_EN.string.SELECT_GEAR
        }
        else if doorNum.isEmpty {
            showStr = R_EN.string.ENTER_ROOM_NUMBER
        }
        
        else if selectedCarType.isEmpty {
            showStr = R_EN.string.SELECT_CARTYPE
        }
         
        else if selectedPeriod == 0 {
            showStr = R_EN.string.ENTER_PRODUCT_PRICETYPE
        }
        
        else if priceTxt.isEmpty {
            showStr = R_EN.string.ENTER_PRODUCT_PRICE
        }
        
        else if depositState.isEmpty {
            showStr = R_EN.string.SELECT_DEPOSIT
        }
            
        else if ui_uploadImg.image == nil {
            showStr = R_EN.string.SELECT_PRODUCT_IMAGE
        }
            
            
        else if addrTxt.isEmpty {
            showStr = R_EN.string.SELECT_ADDR
        }
        
        if showStr.isEmpty {
            let uploadImg = savetoPngFile(image: ui_uploadImg.image!)!
            
            gotoUploadCarApi(title: titleTxt, fuel : selectedFuel, gear: selectedGear, door: doorNum, carType : selectedCarType, priceType : priceOption[selectedPeriod], priceVal : priceTxt, deposition : depositState, description : desciptTxt, image : uploadImg, addr : addrTxt, lat : lat, lng : lng, zipCode: zipCode)
            
        } else {
            showToast(showStr, duration: 1, position: .top)
            return
        }
    }
    
    func gotoUploadCarApi(title: String, fuel : String, gear: String, door: String, carType : String, priceType : String, priceVal : String, deposition : String, description : String, image : String, addr : String, lat : String, lng : String, zipCode: String) {
        
        self.showHUD()
        
        //call api
        
        ProductApiManager.uploadProductApi(title: title, catagoryID: "2", room: "", heeating: "", furbished: "", fuel: fuel, gear: gear, door: door, car: carType, bed: "", person: "", captan: "", gender: "", size: "", color: "", price: priceVal, priceType: priceType, deposit: deposition, description: description, image: image, addr: addr, lat: lat, lng: lng, zipCode: zipCode, completion:  {(isSuccess, data) in
            
            self.hideHUD()
            
            if (isSuccess) {
                
                //let uploadInfo = JSON(data!)
                
                //
                let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_UPLOAD, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: R_EN.string.CANCEL, style: .default, handler: {(action) -> Void in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                
                alert.addAction(UIAlertAction(title: R_EN.string.OK, style: .default, handler : {(action) -> Void in
                   
                    self.setFormData()
                }))
                
                DispatchQueue.main.async(execute:  {
                    self.present(alert, animated: true, completion: nil)
                })
                
            } else {
                
                if data == nil { self.showToast(R_EN.string.CONNECT_FAIL) }
                else { self.showToast(JSON(data!).stringValue) }
            }
        })
        
    }
    
    func setFormData() {
        ui_titleTxt.text = ""
        ui_priceTxt.text = ""
        ui_descriptionTxtView.text = ""
        ui_addrLbl.text = ""
        ui_uploadImg.image = nil
        ui_uploadImgBut.setTitle(R_EN.string.SELECT_UPLOAD_IMAGAE, for: .normal)
        ui_dayRadio.isOn = false
        ui_weekRadio.isOn = false
        ui_monthRadio.isOn = false
//        ui_depositDropDown.select(0)
        selectedPeriod = 0

        ui_doorTxt.text = ""
//        ui_fuelDropDown.select(0)
//        ui_gearDropDown.select(0)
//        ui_cartypeDropDown.select(0)
    }
    
    @IBAction func zoomToCurrentLocation(_ sender: UIBarButtonItem) {
        let span = MKCoordinateSpan.init(latitudeDelta: 0.0075, longitudeDelta: 0.0075)
        let region = MKCoordinateRegion.init(center: (locationManager.location?.coordinate)!, span: span)
        ui_mapView.setRegion(region, animated: true)
    }
    
    @IBAction func mapTypeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case MapType.StandardMap.rawValue:
            ui_mapView.mapType = .standard
        case MapType.SatelliteMap.rawValue:
            ui_mapView.mapType = .satellite
        case MapType.HybridMap.rawValue:
            ui_mapView.mapType = .hybrid
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
