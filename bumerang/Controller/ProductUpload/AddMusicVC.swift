//
//  AddMusicVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/11.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import GDCheckbox
import SwiftyJSON
import Photos
import DKImagePickerController
import DKCamera

class AddMusicVC: BaseViewController {

    var selectedPeriod = 0
    
    @IBOutlet weak var ui_titleTxt: UITextField!
    @IBOutlet weak var ui_priceTxt: UITextField!
    @IBOutlet weak var ui_dayRadio: GDCheckbox!
    @IBOutlet weak var ui_weekRadio: GDCheckbox!
    @IBOutlet weak var ui_monthRadio: GDCheckbox!
    @IBOutlet weak var ui_descriptionTxtView: UITextView!
    @IBOutlet weak var ui_uploadImg: UIImageView!
    @IBOutlet weak var ui_uploadImgBut: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onCheckBoxPress(_ sender: GDCheckbox) {
        getPeriodVal(sender.tag)
    }
    
    @IBAction func onClickDaily(_ sender: Any) {
        ui_dayRadio.isOn = true
        getPeriodVal(1)
    }
    
    @IBAction func onClickWeek(_ sender: Any) {
        ui_weekRadio.isOn = true
        getPeriodVal(2)
    }
    
    @IBAction func onClickMonth(_ sender: Any) {
        ui_monthRadio.isOn = true
        getPeriodVal(3)
    }
    
    func getPeriodVal(_ val : Int) {
        
        selectedPeriod = val
        
        if val == 1 {
            ui_weekRadio.isOn = false
            ui_monthRadio.isOn = false
        } else if val == 2 {
            ui_dayRadio.isOn = false
            ui_monthRadio.isOn = false
        } else if val == 3 {
            ui_dayRadio.isOn = false
            ui_weekRadio.isOn = false
        }
    }
    
    @IBAction func onClickUploadImg(_ sender: UIButton) {
        
        if ui_uploadImg.image == nil {
            self.selectImage()
            
        } else {
            let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_DELECT_IMAGE, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: R_EN.string.CANCEL, style: .default, handler: nil))
            
            alert.addAction(UIAlertAction(title: R_EN.string.OK, style: .default, handler : {(action) -> Void in
                self.ui_uploadImg.image = nil
                sender.setTitle(R_EN.string.SELECT_PRODUCT_IMAGE, for: .normal)
            }))
            
            DispatchQueue.main.async(execute:  {
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    func selectImage(){
        
        let galleryAction = UIAlertAction(title: "from gallery", style: .destructive) { (action) in
            self.openGallery()
        }
        let cameraAction = UIAlertAction(title: "from camera", style: .destructive) { (action) in
            self.openCamera()
        }
        let cancelAction = UIAlertAction(title:R_EN.string.CANCEL, style: .cancel, handler : nil)
        
        // Create and configure the alert controller.
        let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_IMAGE_SETMODE, preferredStyle: .actionSheet)
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openGallery() {
        let pickerController = DKImagePickerController()
        pickerController.assetType = .allAssets
        pickerController.allowSwipeToSelect = true
        pickerController.sourceType = .photo
        pickerController.singleSelect = true
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.didSelectAssets = {(assets: [DKAsset]) in self.onSelectAssets(assets: assets)
        }
        
        present(pickerController, animated: true, completion : nil)
    }
    
    func openCamera(){
        
        let camera = DKCamera()
        camera.allowsRotate = true
        camera.showsCameraControls = true
        camera.defaultCaptureDevice = DKCameraDeviceSourceType.rear
        
        camera.didCancel = {self.dismiss(animated: true, completion: nil)}
        camera.didFinishCapturingImage = { (image: UIImage?, metadata: [AnyHashable : Any]?) in
            self.dismiss(animated: true, completion: nil)
            
            if let img = image {
                self.ui_uploadImg.image = img
                self.ui_uploadImgBut.setTitle("", for: .normal)
            }
        }
        
        present(camera, animated: true, completion: nil)
    }
    
    func onSelectAssets(assets : [DKAsset]) {
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    if let img = image {
                        self.ui_uploadImg.image = img
                        self.ui_uploadImgBut.setTitle("", for: .normal)
                    }
                })
            }
        }
        else {}
    }
    
    @IBAction func onClickUpload(_ sender: Any) {
        
        var showStr = ""
        
        let titleTxt = ui_titleTxt.text!
        let priceTxt = ui_priceTxt.text!
        let desciptTxt = ui_descriptionTxtView.text!
        
        if titleTxt.isEmpty {
            showStr = R_EN.string.ENTER_PRODUCT_TITLE
        }
            
            
        else if selectedPeriod == 0 {
            showStr = R_EN.string.ENTER_PRODUCT_PRICETYPE
        }
        else if priceTxt.isEmpty {
            showStr = R_EN.string.ENTER_PRODUCT_PRICE
        }
        else if ui_uploadImg.image == nil {
            showStr = R_EN.string.SELECT_PRODUCT_IMAGE
        }
        
        if showStr.isEmpty {
            
            let uploadImg = savetoPngFile(image: ui_uploadImg.image!)!
            
            doGotoUploadMusicApi(title: titleTxt, priceType: priceOption[selectedPeriod], price: priceTxt, description: desciptTxt, image: uploadImg)
        } else {
            showToast(showStr, duration: 1, position: .top)
            return
        }
    }
    
    func doGotoUploadMusicApi(title: String, priceType: String, price : String, description : String, image: String) {
        
        self.showHUD()
        
        //call api
        ProductApiManager.uploadProductApi(title: title, catagoryID: "10", room: "", heeating: "", furbished: "", fuel: "", gear: "", door: "", car: "", bed: "", person: "", captan: "", gender: "", size: "", color: "", price: price, priceType: priceType, deposit: "", description: description, image: image, addr: "", lat: "", lng: "", zipCode: "", completion:  {(isSuccess, data) in
            
            self.hideHUD()
            
            if (isSuccess) {
                let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_UPLOAD, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: R_EN.string.CANCEL, style: .default, handler: {(action) -> Void in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                
                alert.addAction(UIAlertAction(title: R_EN.string.OK, style: .default, handler : {(action) -> Void in
                    
                    self.setFormData()
                }))
                
                DispatchQueue.main.async(execute:  {
                    self.present(alert, animated: true, completion: nil)
                })
                
            } else {
                
                if data == nil { self.showToast(R_EN.string.CONNECT_FAIL) }
                else { self.showToast(JSON(data!).stringValue) }
            }
        })
        
    }
    
    func setFormData() {
        ui_titleTxt.text = ""
        ui_priceTxt.text = ""
        ui_descriptionTxtView.text = ""
        ui_uploadImg.image = nil
        ui_uploadImgBut.setTitle(R_EN.string.SELECT_UPLOAD_IMAGAE, for: .normal)
        ui_dayRadio.isOn = false
        ui_weekRadio.isOn = false
        ui_monthRadio.isOn = false
        selectedPeriod = 0
        
    }
    

}
