
import UIKit
import iOSDropDown
import GDCheckbox
import MapKit

import Photos
import DKImagePickerController
import DKCamera
import SwiftyJSON
import SwiftyUserDefaults

class AddApartmentFlatVC: BaseViewController {
    
    var selectedPeriod = 0
    var heatingState = ""
    var furbishedState = ""
    var depositState = ""
    var lan : Float = 0
    var long : Float = 0
    
    @IBOutlet weak var ui_titleTxt: UITextField!
    @IBOutlet weak var ui_roomNumber: UITextField!
    @IBOutlet weak var ui_heatingDropDown: DropDown!
    @IBOutlet weak var ui_furbishedDropDown: DropDown!
    @IBOutlet weak var ui_priceTxt: UITextField!
    @IBOutlet weak var ui_depositDropDown: DropDown!
    @IBOutlet weak var ui_descriptiontxtView: UITextView!
    
    @IBOutlet weak var ui_dayRadio: GDCheckbox!
    @IBOutlet weak var ui_weekRadio: GDCheckbox!
    @IBOutlet weak var ui_monthRadio: GDCheckbox!
    
    @IBOutlet weak var ui_uploadImg: UIImageView!
    @IBOutlet weak var ui_addrLbl: UITextField!
    @IBOutlet weak var ui_mapview: MKMapView!
    @IBOutlet weak var ui_uploadImgBut: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let heatingOption = HeatingOption()
        ui_heatingDropDown.optionArray = heatingOption.values
        ui_heatingDropDown.optionIds = heatingOption.ids
        ui_heatingDropDown.checkMarkEnabled = false
        ui_heatingDropDown.didSelect{(selectedText , index , id) in
            self.heatingState = selectedText
        }
        
        let boolTypeOption = BoolTypeOption()
        ui_furbishedDropDown.optionArray = boolTypeOption.values
        ui_furbishedDropDown.optionIds = boolTypeOption.ids
        ui_furbishedDropDown.checkMarkEnabled = false
//        ui_furbishedDropDown.optionImageArray = ["ic_address_grey","ic_address_grey"]
        ui_furbishedDropDown.didSelect{(selectedText , index , id) in
            self.furbishedState = selectedText
        }
        
        ui_depositDropDown.optionArray = boolTypeOption.values
        ui_depositDropDown.optionIds = boolTypeOption.ids
        ui_depositDropDown.checkMarkEnabled = false
        ui_depositDropDown.didSelect{(selectedText , index , id) in
            self.depositState = selectedText
        }
        
    }
    
    @IBAction func onCheckBoxPress(_ sender: GDCheckbox) {
        getPeriodVal(sender.tag)
    }
    
    @IBAction func onClickDaily(_ sender: Any) {
        ui_dayRadio.isOn = true
        getPeriodVal(1)
    }
    
    @IBAction func onClickWeek(_ sender: Any) {
        ui_weekRadio.isOn = true
        getPeriodVal(2)
    }
    
    @IBAction func onClickMonth(_ sender: Any) {
        ui_monthRadio.isOn = true
        getPeriodVal(3)
    }
    
    func getPeriodVal(_ val : Int) {
        
        selectedPeriod = val
        
        if val == 1 {
            ui_weekRadio.isOn = false
            ui_monthRadio.isOn = false
        } else if val == 2 {
            ui_dayRadio.isOn = false
            ui_monthRadio.isOn = false
        } else if val == 3 {
            
            ui_dayRadio.isOn = false
            ui_weekRadio.isOn = false
        }
        
    }

    @IBAction func onClickUploadImg(_ sender: UIButton) {
        
        if ui_uploadImg.image == nil {
            selectImage()
        } else {
            
            let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_DELECT_IMAGE, preferredStyle: .alert)
            //preferredStyle: .actionSheet
            alert.addAction(UIAlertAction(title: R_EN.string.CANCEL, style: .default, handler: nil))
            
            alert.addAction(UIAlertAction(title: R_EN.string.OK, style: .default, handler : {(action) -> Void in
                self.ui_uploadImg.image = nil
                sender.setTitle(R_EN.string.SELECT_UPLOAD_IMAGAE, for: .normal)
            }))
            
            DispatchQueue.main.async(execute:  {
                self.present(alert, animated: true, completion: nil)
            })
            
        }
    }
    
    func selectImage(){
        
        let galleryAction = UIAlertAction(title: "from gallery", style: .destructive) { (action) in
            self.openGallery()
        }
        let cameraAction = UIAlertAction(title: "from camera", style: .destructive) { (action) in
            self.openCamera()
        }
        let cancelAction = UIAlertAction(title:R_EN.string.CANCEL, style: .cancel, handler : nil)
        
        // Create and configure the alert controller.
        let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_IMAGE_SETMODE, preferredStyle: .actionSheet)
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openGallery() {
        let pickerController = DKImagePickerController()
        pickerController.assetType = .allAssets
        pickerController.allowSwipeToSelect = true
        pickerController.sourceType = .photo
        pickerController.singleSelect = true
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.didSelectAssets = {(assets: [DKAsset]) in
            self.onSelectAssets(assets: assets)
        }
        
        present(pickerController, animated: true, completion : nil)
    }
    
    func openCamera(){
        
        let camera = DKCamera()
        camera.allowsRotate = true
        camera.showsCameraControls = true
        camera.defaultCaptureDevice = DKCameraDeviceSourceType.rear
        
        camera.didCancel = {self.dismiss(animated: true, completion: nil)}
        camera.didFinishCapturingImage = { (image: UIImage?, metadata: [AnyHashable : Any]?) in
            self.dismiss(animated: true, completion: nil)
            
            if let img = image {
                self.ui_uploadImg.image = img
                self.ui_uploadImgBut.setTitle("", for: .normal)
            }
        }
        
        present(camera, animated: true, completion: nil)
    }
    
    func onSelectAssets(assets : [DKAsset]) {
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    if let img = image {
                        self.ui_uploadImg.image = img
                        self.ui_uploadImgBut.setTitle("", for: .normal)
                    }
                })
            }
        }
        else {}
    }
    
    @IBAction func onClickUpload(_ sender: Any) {
        
        var showStr = ""
        
        let titleTxt = ui_titleTxt.text!
        let roomNumTxt = ui_roomNumber.text!
        let priceTxt = ui_priceTxt.text!
        let desciptTxt = ui_descriptiontxtView.text!
        let addr = ui_addrLbl.text!
        let lat = String(ui_mapview.centerCoordinate.latitude)
        let lng = String(ui_mapview.centerCoordinate.longitude)
        let zipCode = "123456"
        
        if titleTxt.isEmpty {
            showStr = R_EN.string.ENTER_PRODUCT_TITLE
        }
        else if roomNumTxt.isEmpty {
            showStr = R_EN.string.ENTER_ROOM_NUMBER
        }
        else if heatingState.isEmpty {
            showStr = R_EN.string.SELECT_HOUSE_HEATING
        }
        else if furbishedState.isEmpty {
            showStr = R_EN.string.SELECT_HOUSE_FURBISHED
        }
        else if selectedPeriod == 0 {
            showStr = R_EN.string.ENTER_PRODUCT_PRICETYPE
        }
        else if priceTxt.isEmpty {
            showStr = R_EN.string.ENTER_PRODUCT_PRICE
        }
        else if depositState.isEmpty {
            showStr = R_EN.string.SELECT_DEPOSIT
        }
        else if addr.isEmpty {
            showStr = R_EN.string.SELECT_ADDR
        }
            
        else if ui_uploadImg.image == nil {
            showStr = R_EN.string.SELECT_PRODUCT_IMAGE
        }
        
        if showStr.isEmpty {
            
            let uploadImg = savetoPngFile(image: ui_uploadImg.image!)!
            
            gotoUploadApi(title: titleTxt, roomnum: roomNumTxt, heating: heatingState, furbished:furbishedState, priceType: priceOption[selectedPeriod], price: priceTxt, deposit: depositState, description: desciptTxt, image: uploadImg, addr: addr, lat: lat, lng: lng, zipCode: zipCode)
        
        } else {
            showToast(showStr, duration: 1, position: .top)
            return
        }
            
    }
    
    func gotoUploadApi(title: String, roomnum : String, heating: String, furbished:String, priceType: String, price : String, deposit: String, description : String, image: String, addr : String, lat : String, lng : String, zipCode: String) {
    
        self.showHUD()
        
        //call api
        ProductApiManager.uploadProductApi(title: title, catagoryID: "1", room: roomnum, heeating: heating, furbished: furbished, fuel: "", gear: "", door: "", car: "", bed: "", person: "", captan: "", gender: "", size: "", color: "", price: price, priceType: priceType, deposit: deposit, description: description, image: image, addr: addr, lat: lat, lng: lng, zipCode: zipCode, completion:  {(isSuccess, data) in
            
            self.hideHUD()
            
            if (isSuccess) {
                
                //let uploadInfo = JSON(data!)
                
                let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_UPLOAD, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: R_EN.string.CANCEL, style: .default, handler: {(action) -> Void in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                
                alert.addAction(UIAlertAction(title: R_EN.string.OK, style: .default, handler: {(action) -> Void in
                    self.setFormData()
                }))
                
                DispatchQueue.main.async(execute:  {
                    self.present(alert, animated: true, completion: nil)
                })
                
            } else {
                
                if data == nil { self.showToast(R_EN.string.CONNECT_FAIL) }
                else { self.showToast(JSON(data!).stringValue) }
            }
        })
        
    }
    
    
    func setFormData() {
        self.view.layoutIfNeeded()
        self.view.setNeedsDisplay()
    
        
//        ui_titleTxt.text = ""
//        ui_priceTxt.text = ""
//        ui_descriptiontxtView.text = ""
//        ui_addrLbl.text = ""
//        ui_uploadImg.image = nil
//        ui_uploadImgBut.setTitle(R_EN.string.SELECT_UPLOAD_IMAGAE, for: .normal)
//        ui_dayRadio.isOn = false
//        ui_weekRadio.isOn = false
//        ui_monthRadio.isOn = false
//        ui_depositDropDown.select(0)
//        selectedPeriod = 0
//
//        ui_roomNumber.text = ""
//        ui_heatingDropDown.select(0)
//        ui_furbishedDropDown.select(0)
    }
}
