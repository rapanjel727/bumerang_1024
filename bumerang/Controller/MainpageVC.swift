
import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import UPCarouselFlowLayout
import iOSDropDown

class MainpageVC: BaseViewController {
    
    @IBOutlet weak var ui_searchBar: UISearchBar!
    @IBOutlet weak var ui_collection_cat: UICollectionView!
    @IBOutlet weak var ui_collection_ads: UICollectionView!
    @IBOutlet weak var ui_collection_filter: UICollectionView!
    @IBOutlet weak var ui_collection_prod: UICollectionView!
    
    @IBOutlet weak var ui_view_coll_ads: UIView!
    @IBOutlet weak var ui_view_coll_filter: UIView!
    @IBOutlet weak var ui_lbl_chatnum: UILabel!
    @IBOutlet weak var ui_lbl_boxnum: UILabel!
    
    var catagoryData = [MainCatagoryModel]()
    var adsData     = [MainAdsModel]()
    var productData = [MainProductModel]()
    
    var allFilterData  = [[MainFilterModel]()]
    var oneFilterCell = [MainFilterModel]()
//    var FilterAlldata = [[MainFilterModel]()]()
    
    var priOffsetY : CGFloat = 0
    var currentCatagory = 0
    var currentAdsPage = 0
    var adsHeight : CGFloat = 0
    var preSelectedCatagoryId = -1
    
    // product filter
    var roomNum = ""
    var heating = ""
    var gender = ""
    var size = ""
    var color = ""
    var furbished = ""
    var fuel = ""
    var gear = ""
    var carType = ""
    var bedNum = ""
    var personNum = ""
    var captan = ""
    var price = ""
    var priceType = ""
    var doorNum = ""
    var deposit = ""
    
    var pageSize: CGSize {
        let layout = ui_collection_ads.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        pageSize.width += layout.minimumLineSpacing
        return pageSize
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gotoProductAdsApi(catagoryId: 0, pageNum: 1)
        
        loadCatagoryData()
        setFilterAllData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        ui_collection_cat.reloadData()
        ui_collection_cat.layoutIfNeeded()

        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupLayout() {
        
        let layout = UPCarouselFlowLayout()
//        let layout = self.ui_collection_ads.collectionViewLayout as! UPCarouselFlowLayout
        layout.itemSize = CGSize(width: self.ui_collection_ads.frame.width * 0.6, height: self.ui_collection_ads.frame.height)
        layout.scrollDirection = .horizontal
        layout.spacingMode = .fixed(spacing: 20)
        layout.sideItemScale = 0.9
        layout.sideItemAlpha = 1
        
        ui_collection_ads.collectionViewLayout = layout
        
    }
    
    func setFilterAllData(){
        
        allFilterData.removeAll()
        oneFilterCell.removeAll()
        for index in 0 ..< 11 {
            
            switch index {
                case 0:
                    
                    oneFilterCell.append(MainFilterModel(titleLbl: "Room number", filterLbls: RoomNumberOption().values, filterIDs: RoomNumberOption().ids, cellWidth: CGFloat(120)) )
                    
                    oneFilterCell.append(MainFilterModel(titleLbl: "Heating", filterLbls: HeatingOption().values, filterIDs: HeatingOption().ids, cellWidth: CGFloat(140)))
                    
                    oneFilterCell.append(MainFilterModel(titleLbl: "Furbished", filterLbls: BoolTypeOption().values, filterIDs: BoolTypeOption().ids, cellWidth: CGFloat(90)))
                    
                    oneFilterCell.append(MainFilterModel(titleLbl: "Price", filterLbls: PriceFlatOption().values, filterIDs: PriceFlatOption().ids, cellWidth: CGFloat(110)))
                
            case 1:
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Fuel Type", filterLbls: FuelTypeOption().values, filterIDs: FuelTypeOption().ids, cellWidth: CGFloat(120)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Gear Type", filterLbls: GearOption().values, filterIDs: GearOption().ids, cellWidth: CGFloat(120)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Door Number", filterLbls: RoomNumberOption().values, filterIDs: RoomNumberOption().ids, cellWidth: CGFloat(110)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Car Type", filterLbls: CarTypeOption().values, filterIDs: CarTypeOption().ids, cellWidth: CGFloat(90)))
                
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Price", filterLbls: PriceCarOption().values, filterIDs: PriceCarOption().ids, cellWidth: CGFloat(100)))
            case 2:
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Bed Capacity", filterLbls: RoomNumberOption().values, filterIDs: RoomNumberOption().ids, cellWidth: CGFloat(120)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Fuel Type", filterLbls: FuelTypeOption().values, filterIDs: FuelTypeOption().ids, cellWidth: CGFloat(120)))
                
                oneFilterCell.append(MainFilterModel(titleLbl: "Price", filterLbls: PriceCaravanSeaOption().values, filterIDs: PriceCaravanSeaOption().ids, cellWidth: CGFloat(100)))
            default: break
            }
            
            oneFilterCell.append(MainFilterModel(titleLbl: "Deposit", filterLbls: BoolTypeOption().values, filterIDs: BoolTypeOption().ids, cellWidth: CGFloat(70)))
            
            allFilterData.append(oneFilterCell)
            
        }
        
        oneFilterCell.removeAll()
    }
    
    @IBAction func onClickMyLocation(_ sender: Any) {
        
        Defaults[.rememberState] = false
        self.dissmisAndGotoVC("SigninVC")
//        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onClickPluse(_ sender: Any) {
        self.gotoNavigationScreen("CatagorySelectVC", direction: .fromLeft)
    }
    
    @IBAction func onClickChart(_ sender: Any) {
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "MainpageVC")
        
        self.navigationController?.pushViewController(toVC!, animated: false)
        self.view.setNeedsLayout()
    }
    
    @IBAction func onClickChat(_ sender: Any) {
        self.gotoNavigationScreen("ChatListVC", direction: .fromLeft)
    }
    
    @IBAction func onClickRend(_ sender: Any) {
        self.gotoNavigationScreen("RentHistoryVC", direction: .fromLeft)
    }
    
    @IBAction func onClickMyProfile(_ sender: Any) {
        self.gotoNavigationScreen("UserInfoVC", direction: .fromLeft)
    }
    
    func addAdsCard(_ categoryID : Int){
        
        let adsImage = "ads_background"
        
        if categoryID < 5 {
            if adsData[adsData.count-1].adsImg == adsImage {
                adsData.remove(at: adsData.count-1)
            }
            
            adsData.append(MainAdsModel(adsId: adsData.count, adsImg: adsImage, adsTitle: "", adsDescription: ""))
        } else {}
    }
    
    func loadCatagoryData(){
        for i in 0 ..< Constants.cateImage.count {
            let one = MainCatagoryModel(
                catagoryId: i,
                catatoryImg: Constants.cateImage[i],
                catagoryColor: Constants.cataColor[i],
                catagoryState: false
            )
            catagoryData.append(one)
        }
        
        ui_collection_cat.reloadData()
    }

    func loadFilterData(_ catagoryID: Int) {
        
        oneFilterCell.removeAll()
        oneFilterCell = allFilterData[catagoryID]
        
        ui_collection_filter.reloadData()
        ui_collection_filter.layoutIfNeeded()
    }
    
    func setVisablity(category: Int) {
        
        if category == 0 || category > 5 {
            
            self.ui_collection_ads.visiblity(gone: true, dimension: 0.0)
            self.ui_view_coll_ads.visiblity(gone: true, dimension: 0.0)
            
            self.ui_collection_filter.visiblity(gone: true, dimension: 0.0)
            self.ui_view_coll_filter.visiblity(gone: true, dimension: 0.0)
            
        } else {
            
            self.ui_collection_ads.visiblity(gone: false, dimension: 140.0)
            self.ui_view_coll_ads.visiblity(gone: false, dimension: 150.0)
            
            self.ui_collection_filter.visiblity(gone: false, dimension: 45.0)
            self.ui_view_coll_filter.visiblity(gone: false, dimension: 50.0)
        }
    }
    
    func gotoProductAdsApi(catagoryId : Int, pageNum : Int) {
        
        if preSelectedCatagoryId == catagoryId, pageNum == 1 {
            return
        }
        
        self.showHUD()
        if pageNum == 1 {
            self.adsData.removeAll()
            self.productData.removeAll()
        }
        
        switch catagoryId {
            case 1:
                roomNum = ""
                heating = ""
            default:
                roomNum = ""
        }
        
        setVisablity(category: catagoryId)

        ProductApiManager.getProductList(catagoryId: "\(catagoryId)", pageNum: "\(pageNum)", roomNum: roomNum, heating: heating, gender: gender, size: size, color: color, furbished: furbished, fuel: fuel, gear: gear, carType: carType, bedNum: bedNum, personNum: personNum, captan: captan, price: price, priceType: priceType, doorNum: doorNum, deposit: deposit, completion: {(isSuccess, resProducductData, resAdsData) in
            
            self.hideHUD()
            
            if (isSuccess) {
                
                //product data
                let productList = JSON(resProducductData!).arrayValue
                for one in productList {
                    let oneProduct = MainProductModel(dict: one)
                    self.productData.append(oneProduct)
                }

                //advantage data
                let adsList = JSON(resAdsData!).arrayValue
                for one in adsList {
                    let oneAds = MainAdsModel(dict: one)
                    self.adsData.append(oneAds)
                }

                self.addAdsCard(catagoryId - 1)
                
                if self.adsData.count == 0 {
                    self.ui_collection_ads.isHidden = true
                } else {
                    self.ui_collection_ads.isHidden = false
                }

                if catagoryId > 0, pageNum == 1 {
                    self.loadFilterData(catagoryId - 1)
                }
                
                
            }
            else {
                if resProducductData == nil { self.showToast(R_EN.string.CONNECT_FAIL) }
                else { self.showToast(JSON(resProducductData!).stringValue) }
            }
            
            self.ui_collection_prod.reloadData()
            self.ui_collection_ads.reloadData()
        })
        
        preSelectedCatagoryId = catagoryId
    }
    
    func gotoProdauctdetailpageVC( productNum : Int) {
        
        showToast("\(productData[productNum].category)")
        
        var nameVC = ""
        let catagoryNum = productData[productNum].category
        
        switch  catagoryNum {
        case 1 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeDetailVC") as! HomeDetailVC
            toVC.oneProduct = productData[productNum]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 2 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailVC") as! CarDetailVC
            toVC.oneProduct = productData[productNum]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 3:
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CaravanDetailVC") as! CaravanDetailVC
            toVC.oneProduct = productData[productNum]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 4 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "SeaVehicleDetailVC") as! SeaVehicleDetailVC
            toVC.oneProduct = productData[productNum]
            self.navigationController?.pushViewController(toVC, animated: true)

        case 5 :
            
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "ClothDetailVC") as! ClothDetailVC
            toVC.oneProduct = productData[productNum]
            self.navigationController?.pushViewController(toVC, animated: true)
        case 6:
            
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "BikeDetailVC") as! BikeDetailVC
            toVC.oneProduct = productData[productNum]
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 7 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productNum]
            toVC.navigationTitle = "Camera Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 8 :
            
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productNum]
            toVC.navigationTitle = "Spore Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 9:
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productNum]
            toVC.navigationTitle = "Kamp Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 10 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productNum]
            toVC.navigationTitle = "Music Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        case 11 :
            let toVC = self.storyboard?.instantiateViewController(withIdentifier: "CameraDetailVC") as! CameraDetailVC
            toVC.oneProduct = productData[productNum]
            toVC.navigationTitle = "Other Detail"
            self.navigationController?.pushViewController(toVC, animated: true)
            
        default:
            nameVC = ""
        }
        
        if nameVC.isEmpty {
            return
        }
        
        showToast(nameVC, duration: 1, position: .center)
    }
}

extension MainpageVC : UISearchBarDelegate {
    
    private func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    private func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    
    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        showToast("searchBarSearchButtonClickedlogin", duration: 2, position: .center)
        searchBar.endEditing(true)
    }
}

extension MainpageVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.ui_collection_cat {
            
            currentCatagory = indexPath.row + 1

            for i in 0 ..< catagoryData.count {
                let state = (i == currentCatagory - 1 ? true : false)
                catagoryData[i].catagoryState = state
            }
            ui_collection_cat.reloadData()
            
            self.gotoProductAdsApi(catagoryId: currentCatagory, pageNum: 1)
          
        }
        else if collectionView == self.ui_collection_ads {
            
            //self.ui_collection_ads.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            //currentAdsPage = indexPath.row
            let selectedADSNum = indexPath.row + 1
            if self.adsData.count == selectedADSNum {
                self.showToast("clickAddAds")
            }
            
        } else {
            
            self.gotoProdauctdetailpageVC(productNum: indexPath.row)
        }
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.ui_collection_prod {
            priOffsetY = scrollView.contentOffset.y
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == self.ui_collection_ads {
            let total = scrollView.contentSize.width - scrollView.bounds.width
            let offset = scrollView.contentOffset.x
            let percent = Double(offset / total)
            currentAdsPage = Int(percent * Double(self.adsData.count - 1))
            
        } else if scrollView == self.ui_collection_prod { }
    }
}

extension MainpageVC : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.ui_collection_cat {return catagoryData.count}
        if collectionView == self.ui_collection_ads {return adsData.count}
        if collectionView == self.ui_collection_filter {return oneFilterCell.count}
        return productData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.ui_collection_cat {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageCatCell", for: indexPath) as! MainpageCatCell
            cell.entity = catagoryData[indexPath.row]
            
            return cell
        }
        else if collectionView == self.ui_collection_ads {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageAdsCell", for: indexPath) as! MainpageAdsCell
            if adsData.count > 0 {
                cell.entity = adsData[indexPath.row]
            }
            return cell
        }
        else if collectionView == self.ui_collection_filter {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageFilterCell", for: indexPath) as! MainpageFilterCell
            cell.entity = oneFilterCell[indexPath.row]
            cell.tag = currentCatagory
            cell.ui_dropdownFilter.tag = indexPath.row
            cell.dropDelegate = self
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainpageProductCell", for: indexPath) as! MainpageProductCell
        cell.entity = productData[indexPath.row]
        return cell
    }
}

extension MainpageVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var w : CGFloat = 0
        var h : CGFloat = 0
        
        if collectionView == self.ui_collection_cat {
            h = collectionView.frame.size.height
            w = h
        }
            
        else if collectionView == self.ui_collection_ads {
            w = collectionView.frame.size.width * 0.7
            h = collectionView.frame.size.height
        }
            
        else if collectionView == self.ui_collection_filter {
            w = oneFilterCell[indexPath.row].cellWidth
            h = collectionView.frame.size.height
        }
            
        else if collectionView == self.ui_collection_prod {
            
            w = (collectionView.frame.size.width - 10) / 2
            h = w * 1.1
        }
        
        return CGSize(width: w, height: h)
    }
}

extension MainpageVC : DropdownDelegate {
    
    func onSelect(categoryID: Int, dropIndex: Int, selectedVal: String) {
        
        print("categoryID: \(categoryID), dropIndex: \(dropIndex), selectedVal: \(selectedVal)")
    }
}

/*
extension UIViewController {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)

        if let nav = self.navigationController {
            nav.view.endEditing(true)
        }
    }
    
    func reloadViewFromNib() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view) // This line causes the view to be reloaded
    }
}
*/
