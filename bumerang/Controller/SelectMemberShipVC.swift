//
//  SelectMemberShipVC.swift
//  bumerang
//
//  Created by RMS on 2019/10/8.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import CHIPageControl
import UPCarouselFlowLayout

class SelectMemberShipVC: BaseViewController {
    
    // selected Membership andcategory
    var selectCategory = -1
    var currentPage = 0
    let myMerbership = 0
    
    internal let numberOfPages = 3
    private var progress = 0.0
    
    @IBOutlet var pageControls: [CHIBasePageControl]!
    @IBOutlet weak var ui_collection: UICollectionView!
    @IBOutlet weak var ui_butUpgrade: dropShadowDarkButton!
    @IBOutlet weak var ui_butBack: UIView!
    
    var datasource = [IntroModel]()
    
    let imgNames = ["ic_medal_copper", "ic_medal_sliver", "ic_medal.gold"]
    let topicStr = ["Basic Membership", "Plus Membership", "Gold Membership"]
    let contentStr = [
        "100$\nBasic Membership user can upload 20 products",
        "200$\nPlus Membershi user can upload 60 products",
        "500$\nGold Membership user can upload 200 products"
    ]
    
    var pageSize: CGSize {
        let layout = ui_collection.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        pageSize.width += layout.minimumLineSpacing
        return pageSize
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0 ..< imgNames.count {
            let one = IntroModel(imgName: imgNames[i], topicStr: topicStr[i], contentStr: contentStr[i])
            datasource.append(one)
        }
        
        ui_collection.scrollToItem(at: IndexPath.init(row: Int(currentPage), section: 0) , at: .centeredHorizontally, animated: true)
        
        self.setupLayout()
        self.updateButtonSetting()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        self.pageControls.forEach { (control) in
            control.numberOfPages = self.numberOfPages
            control.progress = Double(currentPage)
        }
        
    }
    
    func setupLayout() {
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: self.ui_collection.frame.width - 80, height: self.ui_collection.bounds.height)
        layout.scrollDirection = .horizontal
        layout.spacingMode = .fixed(spacing: 10)
        layout.sideItemScale = 0.9
        layout.sideItemAlpha = 0.9
        
        ui_collection.collectionViewLayout = layout
    }
    
    @IBAction func onTapedBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapedUpgrade(_ sender: Any) {
        
        if selectCategory >= 0 {
            
            let toVC = self.storyboard?.instantiateViewController( withIdentifier: "SingupSetVC") as! SingupSetVC
            toVC.currentPage = 1
            toVC.modalPresentationStyle = .fullScreen
            toVC.selectedMembership = self.currentPage
            
            self.present(toVC, animated: true, completion: nil)
            
        } else {
            showToast("onTapedUpgrade")
        }
    }
    
    func updateButtonSetting() {
        
        var eventState = true
        var butName = "SELECT"
        
        if selectCategory > -1 {
            butName = "SELECT"
            ui_butBack.isHidden = true
            
        } else {
            ui_butBack.isHidden = false
            
            if currentPage == myMerbership {
                butName = "CURRENT PLAN"
                eventState = false
            } else {
                butName = "UPGRADE"
            }
            
            if eventState {
                ui_butUpgrade.borderWidth = 0
                ui_butUpgrade.backgroundColor = UIColor(named: "primary")
                ui_butUpgrade.setTitleColor(UIColor.white, for: .normal)
            } else {
                ui_butUpgrade.borderWidth = 1
                ui_butUpgrade.backgroundColor = UIColor.white
                ui_butUpgrade.setTitleColor(UIColor.black, for: .normal)
            }
        }
        
        ui_butUpgrade.setTitle(butName, for: .normal)
        ui_butUpgrade.isUserInteractionEnabled = eventState
        
    }
    
}

extension SelectMemberShipVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        cell.entity = datasource[indexPath.row]
        return cell
    }
}

extension SelectMemberShipVC : UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let total = scrollView.contentSize.width - scrollView.bounds.width
        let offset = scrollView.contentOffset.x
        let percent = Double(offset / total)
        progress = percent * Double(self.numberOfPages - 1)
        
        self.pageControls.forEach { (control) in
            control.progress = progress
        }
        
        self.updateButtonSetting()
    }
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = ui_collection.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
        self.updateButtonSetting()
    }
}

