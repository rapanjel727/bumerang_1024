//
//  UserProfileVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/13.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import Photos
import DKImagePickerController
import DKCamera
import SwiftyJSON
import SwiftyUserDefaults

class UserProfileVC: BaseViewController {

    @IBOutlet weak var ui_imgAvatar: UIImageView!
    @IBOutlet weak var ui_txtFname: CustomTextField!
    @IBOutlet weak var ui_lblNameVerify: UILabel!
    @IBOutlet weak var ui_txtMail: CustomTextField!
    
    @IBOutlet weak var ui_lbleMailVerify: UILabel!
    @IBOutlet weak var ui_txtPhone: CustomTextField!
    @IBOutlet weak var ui_lblPhoneVerify: UILabel!
    @IBOutlet weak var ui_txtAddr: CustomTextField!
    @IBOutlet weak var ui_lblAddrSave: UILabel!
    @IBOutlet weak var ui_txtCity: CustomTextField!
    @IBOutlet weak var ui_lblCitysave: UILabel!
    
    @IBOutlet weak var ui_imgFacebook: UIImageView!
    @IBOutlet weak var ui_lblFacebook: UILabel!
    @IBOutlet weak var ui_imgGoogle: UIImageView!
    @IBOutlet weak var ui_lblGoogle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onTapedAvatarAdd(_ sender: Any) {
        let galleryAction = UIAlertAction(title: "from gallery", style: .destructive) { (action) in
            self.openGallery()
        }
        let cameraAction = UIAlertAction(title: "from camera", style: .destructive) { (action) in
            self.openCamera()
        }
        let cancelAction = UIAlertAction(title:R_EN.string.CANCEL, style: .cancel, handler : nil)
        
        // Create and configure the alert controller.
        let alert = UIAlertController(title: R_EN.string.APP_TITLE, message: R_EN.string.CORNFRIM_IMAGE_SETMODE, preferredStyle: .actionSheet)
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onTapedMembership(_ sender: Any) {
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: "SelectMemberShipVC") as! SelectMemberShipVC
        //toVC.selectCategory = sender.tag
        
        toVC.modalPresentationStyle = .fullScreen
        
        self.present(toVC, animated: true, completion: nil)
    }
    
    
    @IBAction func onTapedSave(_ sender: Any) {
        showToast("save", duration: 1, position: .center)
    }
    
    func openGallery() {
        let pickerController = DKImagePickerController()
        pickerController.assetType = .allAssets
        pickerController.allowSwipeToSelect = true
        pickerController.sourceType = .photo
        pickerController.singleSelect = true
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.didSelectAssets = {(assets: [DKAsset]) in
            self.onSelectAssets(assets: assets)
        }
        
        present(pickerController, animated: true, completion : nil)
    }
    
    func openCamera(){
        
        let camera = DKCamera()
        camera.allowsRotate = true
        camera.showsCameraControls = true
        camera.defaultCaptureDevice = DKCameraDeviceSourceType.rear
        
        camera.didCancel = {self.dismiss(animated: true, completion: nil)}
        camera.didFinishCapturingImage = { (image: UIImage?, metadata: [AnyHashable : Any]?) in
            self.dismiss(animated: true, completion: nil)
            
            self.ui_imgAvatar.image = image
            
        }
        
        present(camera, animated: true, completion: nil)
    }
    
    func onSelectAssets(assets : [DKAsset]) {
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    self.ui_imgAvatar.image = image
                })
            }
        }
        else {}
    }
    
    
}
