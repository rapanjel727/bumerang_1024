//
//  ChatRoomVC.swift
//  bumerang
//
//  Created by RMS on 2019/9/13.
//  Copyright © 2019 RMS. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class ChatRoomVC: BaseViewController {
    
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtMsg: UITextField!
    
    var chatList:[ChatRoomModel] = [ChatRoomModel]()
    
    let userId = Defaults[.userId]
    var receiveUserId = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        tblChat.estimatedRowHeight = 100
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
        for index in 0 ..< 6 {
            
            let senderId = (index % 2 == 0 ? userId : receiveUserId)
            let receiverId = (index % 2 == 0 ? receiveUserId : userId)
            var string = "\(index) \n" + "Nice to meet you"
            if index % 2 == 0 {
                
                string += "\n" + "Nice to meet you, Nice to meet you, Nice to meet you,Nice to meet you"
                string += "\n" + "Nice to meet you"
                string += "\n" + "Nice to meet you"
            }
            
            let tmp = ChatRoomModel(senderId: senderId, receiverId: receiverId, strDate: getCurrentDateStr(), strMsg: string, userImageUrl: "")
            
            chatList.append(tmp)
        }
        
        tblChat.reloadData()
    }
    
    @IBAction func onClickSend(_ sender: Any) {
        if checkValid() {
            doSend()
        }
    }
    
    func checkValid() -> Bool {
        
        self.view.endEditing(true)
        
        if txtMsg.text!.isEmpty {
            
            let message = "Message content is empty."
            
            showToast(message)
            
            return false
        }
        return true
    }
    
    func doSend() {
        
        //let timeNow = Int(NSDate().timeIntervalSince1970)
        
        var chatObject = [String: String]()
        
        chatObject["userid"] = "1" 
        chatObject["msgContent"] = txtMsg.text! as String
        
        let chatRoomId = "1_10"
        
        FirebaseAPI.sendMessage(chatObject, chatRoomId) { (status, message) in
            
            if status {
                self.txtMsg.text = ""
                print(message)
            } else {
                print(message)
            }
        }
    }
}

extension ChatRoomVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if chatList[indexPath.row].senderId == self.userId { // sending cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendingCell", for: indexPath) as! SendingCell
            
            cell.setCell(chat: chatList[indexPath.row])
            
            return cell
            
        } else { // receiving cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceivingCell", for: indexPath) as! ReceivingCell
            
            cell.setCell(chat: chatList[indexPath.row])
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    fileprivate func scrollToBottom(){
        let index:IndexPath = IndexPath(row: chatList.count, section: 0)
        tblChat.scrollToRow(at: index, at: .bottom, animated: true)
    }
}

extension ChatRoomVC: UITextFieldDelegate {
    // MARK: - UITextField delegate
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        
//    }
//    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        
//        
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == txtMsg) {
            if (checkValid()) {
                self.doSend()
            }
        }
        
        return true
    }
}
