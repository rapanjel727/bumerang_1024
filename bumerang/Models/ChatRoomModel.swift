//
//  ChatRoomModel.swift
//  bumerang
//
//  Created by RMS on 2019/9/12.
//  Copyright © 2019 RMS. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChatRoomModel {
    
    var senderId = 0
    var receiverId = 0
    var strDate = ""
    var strMsg = ""
    var userImageUrl = ""
    var attachfile = ""
    
    init(senderId : Int, receiverId :Int, strDate :String, strMsg : String, userImageUrl : String) {
        
        self.senderId = senderId
        self.receiverId = receiverId
        self.strDate = strDate
        self.strMsg = strMsg
        self.userImageUrl = userImageUrl
    }
    
    init(dic : JSON){
        
    }
    
}

