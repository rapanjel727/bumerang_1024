//
//  ChatListModel.swift
//  bumerang
//
//  Created by RMS on 2019/9/12.
//  Copyright © 2019 RMS. All rights reserved.
//

import Foundation

class ChatListModel {
    
    var senderId : Int = 0
    var receiverId : Int = 0
    var imgName : String = ""
    var username : String = "User Name"
    var contentStr : String = "Hi, Are you there?"
    var reqDate : String = "1hr ago"
    var unreadNum : Int = 1
    
    init(senderId : Int, receiverId : Int, imgName : String, username : String, contentStr :String, reqDate :String, unreadNum : Int) {
        
        self.senderId = senderId
        self.receiverId = receiverId
        self.imgName = imgName
        self.username = username
        self.contentStr = contentStr
        self.reqDate = reqDate
        self.unreadNum = unreadNum
        
//        var dateStrArr = self.updated_at.split(withMaxLength: 19)
//        self.updated_at = dateStrArr[0]
        
    }
}

